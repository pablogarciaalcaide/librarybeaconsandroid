package com.gwella.androidlibrarybeacongwella.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.gwella.androidlibrarybeacongwella.R;
import com.gwella.androidlibrarybeacongwella.core.HttpController;

/**
 * @author juangra on 14/3/16.
 * Esta Activity se usa para mostrar las acciones de las notificaciones,
 * extiende de AppCompatActivity para que sea compatible con las versiones inferiores de Android.
 *
 * Le paso como extras en el intent el path de la imagen y la url, si existe.
 * Creo una instancia del ImageLoader para poder descargarme la imagen en segundo plano
 * sin cortar el flujo principal de la app.
 */
public class ImageActionActivity extends AppCompatActivity
{
    private ImageView imagen;
    private ImageView imBack;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_activity);

        Intent intent = getIntent();

        final String imagePath = intent.getStringExtra("imagePath");
        final String url = intent.getStringExtra("url");

        imagen = findViewById(R.id.imageView);
        imBack = findViewById(R.id.imageBack);
        progressBar = findViewById(R.id.progressBar);

        imBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if( url.length() > 0 )
        {
            imagen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intentBrowser);
                }
            });
        }

        Glide.with(this)
                .load(imagePath)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imagen);

    }
}
