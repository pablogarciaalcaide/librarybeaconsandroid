package com.gwella.androidlibrarybeacongwella.core;


import android.support.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * @author juangra on 18/1/16.
 */
public class RequestCustom extends JsonObjectRequest
{
    public RequestCustom(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }
  /*  public RequestCustom(int method, String url, String requestBody, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
    {
        super(method, url, requestBody, listener, errorListener);
    }

    public RequestCustom(String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
    {
        super(url, listener, errorListener);
    }

    public RequestCustom(int method, String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
    {
        super(method, url, listener, errorListener);
    }

    public RequestCustom(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
    {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public RequestCustom(String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
    {
        super(url, jsonRequest, listener, errorListener);
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError){
        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
            volleyError = error;
        }

        return volleyError;
    }*/


}
