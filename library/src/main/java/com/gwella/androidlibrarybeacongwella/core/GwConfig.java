package com.gwella.androidlibrarybeacongwella.core;

import android.content.Context;
import android.os.Environment;

/**
 * @author juangra on 3/9/15.
 */
public class GwConfig {


    public static final String DB_NAME = "GCMBeacons.sqlite";

    public static String getPackageName( Context context ){
        return context.getApplicationContext().getPackageName();
    }

    public static String getMyexternaldirectory( Context context ){
        return Environment.getExternalStorageDirectory().toString() + "/Gwella";
    }

    public static String getDbPath( Context context ){
        return context.getDatabasePath(DB_NAME).getPath();
    }
}
