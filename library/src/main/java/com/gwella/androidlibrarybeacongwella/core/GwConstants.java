package com.gwella.androidlibrarybeacongwella.core;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings.Secure;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.gwella.androidlibrarybeacongwella.R;
import com.gwella.androidlibrarybeacongwella.activities.ImageActionActivity;
import com.gwella.androidlibrarybeacongwella.model.GwBeacon;
import com.gwella.androidlibrarybeacongwella.model.GwBeaconActions;
import com.gwella.androidlibrarybeacongwella.model.GwAssociationNotifications;
import com.gwella.androidlibrarybeacongwella.model.GwCampaign;
import com.gwella.androidlibrarybeacongwella.model.GwCountBeaconRegion;
import com.gwella.androidlibrarybeacongwella.model.GwGeofence;
import com.gwella.androidlibrarybeacongwella.model.GwMessages;
import com.gwella.androidlibrarybeacongwella.model.GwNotifyTrigger;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;
import com.gwella.androidlibrarybeacongwella.services.GwServiceMonitoring;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author juangra on 3/9/15.
 */
public class GwConstants
{
    public static boolean isInicialize = false;

    public static ArrayList<GwGeofence> geofenceVisibles = new ArrayList<>();

    private static final String TAG = "GwConstants";
    public static Context context;

    public static final String UUID_COMPANY = "uuid_company";
    public static final String IDENTIFIER_COMPANY = "name_company";
    public static final String URL_ICON_APP = "url_icon";

    //Url de conexion con el servidor Testing
    public static final String URL_DEFECT = "https://development.gwellagcm.com/api/";

    public static final String PROPERTY_TOKEN_GOOGLE = "gwellaTokenGoogle";
//    public static final String PROPERTY_TOKEN_GOOGLE_OLD = "gwellaTokenGoogleOld";
    public static final String PROPERTY_AUTHENTICATION_ID = "Authorization_id";
    public static final String PROPERTY_AUTHENTICATION_EXPIRE = "Authorization_expire";
    public static final String PROPERTY_AUTHENTICATION_DATE = "Authorization_date";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String PROPERTY_EXPIRATION_TIME = "onServerExpirationTimeMs";
    public static final String K_UPDATED = "updated";
    public static final String K_COUNT_USER = "count_user_";

    public static final String MESSAGE_UID = "message_uid";
    public static final String SOURCE_UID = "sourceUID";
    public static final String TYPE_MESSAGE = "type_message";
    public static final String DEVICE_ID = "device_id";

    public static final String NAME_FILE_CONTROL_PUSH = "control_uid_messages";
    public static final String PUSH_UID = "push_uid";

    public static final String FILE_SDCARD_DEVICE_ID = ".l1br3r14gw3ll4.gw";

    //Propiedad que se envia en la intallation, indica si estamos en desarrollo o en produccion.
    public static final String ENVIROMENT_DEVELOPMENT = "development";
    public static final String ENVIROMENT_PRODUCTION = "production";
    public static final String ENVIROMENT_CUSTOM = "custom";

    public static final String K_SOURCE_BEACON = "beacon";
    public static final String K_SOURCE_GEO = "geo";
    public static final String K_SOURCE_PUSH = "push";
    public static final String K_ACTION_ENTER = "enter";
    public static final String K_ACTION_EXIT = "exit";
    public static final String K_ACTION_SHOW = "show";
    public static final String K_ACTION_OPEN = "open";
    public static final String K_ACTION_RECEIVE = "receive";
    public static final String K_ACTION_DISMISS = "dismiss";
    public static final String K_ACTION_DISTANCE = "distance";

    public static final String ADVERTISING_ID = "advertising_id";

    //Google api key para la mensajeria push //TODO: poner fuera de la libreria
    public static final String SENDER_ID = "231617929756";

    //Flag para cuando hay que enviar la actualizacion del registro de google
//    public static boolean sendActualizeTokenGoogle = false;

    //Flags para poder capar a la libreria cuando queramos limitarla en las campañas.
    public static boolean BEACONS_ACTIVE = true; //Cuando capemos los beacons no añadir la verificacion de Bluetooth en la pantalla Main.
    public static boolean LOCATION_ACTIVE = true;
    public static boolean GEOFENCING_ACTIVE = true;

//    public static final String K_TYPE_ACTION= "typeAction";
//    public static final String K_TYPE_ACTION_ENTER= "enter";
//    public static final String K_TYPE_ACTION_EXIT= "exit";
//    public static final String K_TYPE_ACTION_OFFER= "offer";

    public static final String kFechaDefecto = "1970-01-01 01:00:00";
    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat formatEsp = new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat formatEsp_completa = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static SimpleDateFormat formatEsp_hora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static SimpleDateFormat formatSinHora = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat formatSinFecha = new SimpleDateFormat("HH:mm");
    public static String[] formats = new String[] {
            "yyyy-MM-dd",
            "yyyy-M-dd",
            "dd/MM/yyyy",
            "dd/M/yyyy",
            "yyyy.MM.dd",
            "yyyy/MM/dd",
            "yyyy MM dd",
            "dd-MM-yyyy",
            "dd.MM.yyyy",
            "dd MM yyyy",
            "MM/dd/yyyy",
            "MM.dd.yyyy",
            "MM-dd-yyyy",
            "MM dd yyyy",
            "EEE, d MMM yyyy",
            "d 'de' MMM 'de' yyyy",
            "dd 'de' MMM 'de' yyyy",
            "EEE 'de' MMM 'de' yyyy",
            "d 'da' MMM 'de' yyyy",
            "dd 'da' MMM 'de' yyyy",
            "EEE 'da' MMM 'de' yyyy",
            "d 'do' MMM 'de' yyyy",
            "dd 'do' MMM 'de' yyyy",
            "EEE 'do' MMM 'de' yyyy",
            "d 'da' MMM 'da' yyyy",
            "dd 'da' MMM 'da' yyyy",
            "EEE 'da' MMM 'da' yyyy",
            "d 'do' MMM 'do' yyyy",
            "dd 'do' MMM 'do' yyyy",
            "EEE 'do' MMM 'do' yyyy",
            "dd'I'MM'I'yyyy",
            "yyyy'I'MM'I'dd",
            "dd'l'MM'l'yyyy",
            "yyyy'l'MM'l'dd",
            "dd'1'MM'1'yyyy",
            "yyyy'1'MM'1'dd"
    };

    public static final int COUNT_MAX_EXIT_REGION = 5;
    //milisegundos de una semana completa
    public static final int EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;
//    public static final int EXPIRATION_TIME_MS = 20000;

    public static HashMap<String, Boolean> beaconsProcesses = new HashMap();

    public static ArrayList<GwCountBeaconRegion> countBeaconRegions = new ArrayList<>();

    public static ProcesoPrincipal procesoPrincipal;

    public static String mainScreen = null;


    public static void inicializarProcesoPrincipal( Context context )
    {
        if( procesoPrincipal == null )
            procesoPrincipal = ProcesoPrincipal.getInstance(context);

        procesoPrincipal.init();

        if( GwConstants.BEACONS_ACTIVE ) {
            Calendar cal = Calendar.getInstance();
            final Intent intentService = new Intent(context, GwServiceMonitoring.class);
            PendingIntent pintent = PendingIntent.getService(context, 0, intentService, 0);
            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if ( alarm != null )
            {
                alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 10 * 1000, pintent);
            }

//            final BroadcastReceiver bluetoothRecived = new BroadcastReceiver() {
//
//                @Override
//                public void onReceive(Context context, Intent intent) {
//                    final String action = intent.getAction();
//
//                    if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
//                        final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
//                        switch(state) {
//                            case BluetoothAdapter.STATE_OFF:
//                                if(GwServiceMonitoring.isIntentServiceRunning)
//                                    context.stopService(intentService);
//                                break;
//                            case BluetoothAdapter.STATE_TURNING_OFF:
////                                context.stopService(intentService);
//                                break;
//                            case BluetoothAdapter.STATE_ON:
//                                initServiceMonitoring(context, intentService);
//                                break;
//                            case BluetoothAdapter.STATE_TURNING_ON:
//
//                                break;
//                        }
//
//                    }
//                }
//            };
//
//            if( isEnabledBluetooth() )
//            {
                initServiceMonitoring(context, intentService);
//            }
//
//            IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
//            context.registerReceiver(bluetoothRecived, filter1);
        }

    }

    private static void initServiceMonitoring(Context context, Intent intentService)
    {
        if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1)
        {
            context.startForegroundService(intentService);
        } else {
            context.startService(intentService);
        }
    }


    public static void enabledBluetooth()
    {
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED)
        {
            if ( !mBtAdapter.isEnabled() )
            {
                mBtAdapter.enable();
            }
        }
    }

    public static boolean isEnabledBluetooth()
    {
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED)
        {
            return mBtAdapter.isEnabled();
        }
        return false;
    }

    public static void deleteBeaconProcess( GwBeacon beacon )
    {
        String key = beacon.getMajorString() + beacon.getMinorString();
        if( beaconsProcesses.containsKey(key) )
        {
            beaconsProcesses.remove(key);
        }
    }

    public static void upsertBeaconProcess( GwBeacon beacon, boolean status )
    {
        String key = beacon.getMajorString() + beacon.getMinorString();

        if( beaconsProcesses.containsKey(key) )
        {
            beaconsProcesses.remove(key);
            beaconsProcesses.put( key, status );
        }
        else
        {
            beaconsProcesses.put(key, status);
        }

    }

    public static boolean getStatusBeacon( GwBeacon beacon )
    {
        String key = beacon.getMajorString() + beacon.getMinorString();

        if( beaconsProcesses.containsKey(key) )
        {
            return beaconsProcesses.get(key).booleanValue();
        }
        else
        {
            return false;
        }
    }

    //JGCB, nuevos metodos de comprobacion de tablas y campos para la base de datos
    public static boolean existeTabla( SQLiteDatabase db,String tabla ){
        Cursor rs = db.query("sqlite_master", new String[]{"name"}, "type='table' AND name='" + tabla + "'", null, null, null, null);

        try{
            if (rs.moveToFirst()) {
                return true;
            }
            else return false;
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        finally{
            rs.close();
        }

        return false;
    }


    public static boolean existeCampo( SQLiteDatabase db, String tabla, String campo ){
        Cursor rs = db.query("sqlite_master", new String[]{"sql"}, "type='table' AND name='" + tabla + "'", null, null, null, null);

        try{
            if (rs.moveToFirst()) {
                return rs.getString(0).indexOf( campo  ) > 0 ? true : false;
            }
            else return false;
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        finally{
            rs.close();
        }

        return false;
    }

    //Descarga el icono de la app, deberia guardarse en un ficheropara poder abrirlo sin tener que descargarlo siempre.
    public static Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sendNotificationPush( Context context, String title, String body, String push_id, int idNotify, String uidMessage, String message, String campaignUid ) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, "default")
                        .setContentTitle(title)
                        .setContentText(body)
                        .setSmallIcon(R.drawable.notificacion)
                        .setPriority(Notification.PRIORITY_HIGH);

        String imageNotifPath = getImageNotifPath(message);
        if( imageNotifPath != null )
        {
            Bitmap bitmap = getBitmapFromURL(imageNotifPath);
            builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).setSummaryText(body));
        }
        else
        {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(body).setBigContentTitle(title));
        }

        String urlIcon = getSharedPreferences(context, URL_ICON_APP);
        if( urlIcon.length() > 2 )
        {
            Bitmap bitmap = getBitmapFromURL(urlIcon);
            if(bitmap != null )
            {
                builder.setLargeIcon(bitmap);
            }
        }

//        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.custom_notification);//Maximo de tamaño 256dp height
////        contentView.setImageViewResource(R.id.image, R.drawable.logo_gwella);
//        contentView.setImageViewBitmap(R.id.image, getBitmapFromURL("https://fruteriadevalencia.com/wp-content/uploads/2015/02/BLANQUILLA-buena.jpg"));
//        contentView.setTextViewText(R.id.title, title);
//        contentView.setTextViewText(R.id.text, body);
//        builder.setCustomBigContentView(contentView);
        
        String className;
        if( mainScreen != null )
        {
            className = context.getPackageName() + "." + mainScreen;
        }
        else
        {
            className = getNameActivityMain(context);
        }
        try
        {
            Intent intent = new Intent(context, Class.forName(className));

            intent.putExtra(MESSAGE_UID, uidMessage);
            intent.putExtra(TYPE_MESSAGE, "push");
            intent.putExtra("push_uid", push_id);
            intent.putExtra("message", message);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, idNotify, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(pendingNotificationIntent);

            String sound = getSound(message);
            if( sound != null )
            {
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
                Uri soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + context.getResources().getIdentifier(sound, "raw", context.getPackageName()));
                builder.setSound(soundUri);
            }
            else
            {
                builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            }
            builder.setAutoCancel(true);
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if(Build.VERSION.SDK_INT >= 26 )
            {
                NotificationChannel channel = new NotificationChannel("default", "ngagemob", NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(idNotify, builder.build());

            setSharedPreferences(context, K_COUNT_USER + campaignUid, getSharedPreferencesInt(context, K_COUNT_USER + campaignUid) + 1 );

        }catch ( ClassNotFoundException e )
        {
            e.printStackTrace();
        }
        //Esto es para que se encienda la pantalla si esta apagada
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        Log.e("screen on......", "" + isScreenOn);
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(10000);
        }
    }

    public static void sendNotification( Context context, String title, String body, int idNotify, String uidMessage, String uidSource, String source ) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, "default")
                        .setContentTitle(title)
                        .setContentText(body)
                        .setSmallIcon(R.drawable.notificacion)
                        .setPriority(Notification.PRIORITY_HIGH);

        GwMiAdapterBD db = GwMiAdapterBD.getInstance(context);
        GwMessages messages = db.selectMessage(uidMessage);

        String imageNotifPath = messages.getImageNotifPath();
        if( imageNotifPath != null && imageNotifPath.length() > 4 )
        {
            Bitmap bitmap = getBitmapFromURL(imageNotifPath);
            builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).setSummaryText(body));
        }
        else
        {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(body).setBigContentTitle(title));
        }

        String urlIcon = getSharedPreferences(context, URL_ICON_APP);
        if( urlIcon.length() > 2 )
        {
            Bitmap bitmap = getBitmapFromURL(urlIcon);
            if(bitmap != null )
            {
                builder.setLargeIcon(bitmap);
            }
        }

        String className;
        if( mainScreen != null )
        {
            className = context.getPackageName() + "." + mainScreen;
        }
        else
        {
            className = getNameActivityMain(context);
        }
        try
        {
            Intent intent = new Intent(context, Class.forName(className));

            intent.putExtra(MESSAGE_UID, uidMessage);
            intent.putExtra(TYPE_MESSAGE, source);
            intent.putExtra(SOURCE_UID, uidSource);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, idNotify, intent ,PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(pendingNotificationIntent);

            String sound = messages.getSound();
            if( sound != null && !sound.isEmpty() )
            {
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
                Uri soundUri = Uri.parse("android.resource://" + context.getPackageName() + "/" + context.getResources().getIdentifier(sound, "raw", context.getPackageName()));
                builder.setSound(soundUri);
            }
            else
            {
                builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            }
            builder.setAutoCancel(true);
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if(Build.VERSION.SDK_INT >= 26 )
            {
                NotificationChannel channel = new NotificationChannel("default", "ngagemob", NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(idNotify, builder.build());

            setSharedPreferences(context, K_COUNT_USER + messages.getCampaign(), getSharedPreferencesInt(context, K_COUNT_USER + messages.getCampaign()) + 1 );

        }catch ( ClassNotFoundException e )
        {
            e.printStackTrace();
        }
        //Esto es para que se encienda la pantalla si esta apagada
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        Log.e("screen on......", "" + isScreenOn);
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(10000);
        }
    }

    //Este metodo sirve para que me devuelva el nombre de la actividad principal de la aplicacion
    public static String getNameActivityMain( Context context )
    {
        String packageName = context.getPackageName();
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        return launchIntent.getComponent().getClassName();
    }

    public static boolean hasSharedPreferences( Context context, String key )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        return  preferences.contains(key);
    }

    public static boolean hasSharedPreferences( Context context, String key, String nameFile )
    {
        SharedPreferences preferences = context.getSharedPreferences(nameFile, Context.MODE_PRIVATE);
        return  preferences.contains(key);
    }

    public static String getSharedPreferences( Context context, String key )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        return  preferences.getString(key, "");
    }

    public static void setSharedPreferences( Context context, String key, String valor )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString( key, valor );
        editor.commit();
    }

    public static String getSharedPreferences( Context context, String key, String nameFile )
    {
        SharedPreferences preferences = context.getSharedPreferences(nameFile, Context.MODE_PRIVATE);
        return  preferences.getString(key, "");
    }

    public static void clearSharedPreferences( Context context, String nameFile)
    {
        SharedPreferences preferences = context.getSharedPreferences(nameFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void setSharedPreferences( Context context, String key, String valor, String nameFile )
    {
        SharedPreferences preferences = context.getSharedPreferences(nameFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString( key, valor );
        editor.commit();
    }

    public static String getRegistrationIdGoogle( Context context )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        return  preferences.getString(PROPERTY_TOKEN_GOOGLE, "");
    }

    public static void setRegistrationIdGoogle( Context context, String valor )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PROPERTY_TOKEN_GOOGLE, valor );
        editor.commit();
    }

    public static int getSharedPreferencesInt( Context context, String key )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        return  preferences.getInt(key, 0);
    }

    public static void setSharedPreferences( Context context, String key, int valor )
    {
        SharedPreferences preferences = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, valor);
        editor.commit();
    }

    public static String encodeBase64( String valor )
    {
        byte[] data = null;
        try {
            data = valor.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }

    public static String decodeBase64( String valor )
    {
        byte[] data1 = Base64.decode(valor, Base64.DEFAULT);
        String text1 = null;
        try {
            text1 = new String(data1, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
        return text1;
    }

    public static Date getPrimerDiaDelMes() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH), cal.getMinimum(Calendar.HOUR_OF_DAY), cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
        return cal.getTime();
    }

    public static Date getUltimoDiaDelMes() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMaximum(Calendar.DAY_OF_MONTH), cal.getMaximum(Calendar.HOUR_OF_DAY), cal.getMaximum(Calendar.MINUTE), cal.getMaximum(Calendar.SECOND));
        return cal.getTime();
    }

    public static void verifyBluetooth( final Activity activity ) {

        try {
            if (!BeaconManager.getInstanceForApplication(activity).checkAvailability()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivity(enableBtIntent);
            }
        }
        catch (RuntimeException e) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle("Bluetooth LE not available");
            builder.setMessage("Sorry, this device does not support Bluetooth LE.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                @Override
                public void onDismiss(DialogInterface dialog) {
                    activity.finish();
                    System.exit(0);
                }

            });
            builder.show();

        }

    }

    public static void checkBeaconExitRegion( ArrayList<Beacon> beaconArrayList )
    {
        boolean existe;

        if( GwConstants.countBeaconRegions.size() <= 0 )
        {
            for( Beacon beacon : beaconArrayList )
            {
                GwConstants.countBeaconRegions.add(new GwCountBeaconRegion( 0, 0, beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString() ));
            }
        }
        else
        {
            //Añadir los que no estuviesen en el array
            for( Beacon beacon : beaconArrayList )
            {
                existe = false;
                for( GwCountBeaconRegion countBeaconR : GwConstants.countBeaconRegions )
                {
                    if( beacon.getId1().toString().equals(countBeaconR.getUuid()) &&
                            beacon.getId2().toString().equals(countBeaconR.getMajor()) &&
                            beacon.getId3().toString().equals(countBeaconR.getMinor()) )
                    {
                        existe = true;
                    }
                }
                if( !existe )
                {
                    GwConstants.countBeaconRegions.add(new GwCountBeaconRegion(0, 0, beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString() ));
                }
            }

            for( GwCountBeaconRegion countBeaconRegion : GwConstants.countBeaconRegions )
            {
                existe = false;
                for( Beacon beacon : beaconArrayList )
                {
                    if( countBeaconRegion.compareTo(beacon) == 0 )
                    {
                        existe = true;
                    }
                }

                if( !existe )
                {
                    countBeaconRegion.setCountExitRegion(countBeaconRegion.getCountExitRegion() + 1);
                }
                else
                {
                    countBeaconRegion.setCountExitRegion(0);
                }
            }

            //Comprobar si algún beacon ha llegado a su maximo de salida para hacer el exit region
            ArrayList<GwCountBeaconRegion> auxCountBeaconRegion = GwConstants.countBeaconRegions;
            for( int i = 0; i < auxCountBeaconRegion.size(); i++ )
            {
                if( auxCountBeaconRegion.get(i).getCountExitRegion() >= GwConstants.COUNT_MAX_EXIT_REGION )
                {
                    exitBeacon( auxCountBeaconRegion.get(i) );
                    GwConstants.countBeaconRegions.remove(i);
                }
            }

        }


    }

    public static void exitBeacon( final GwCountBeaconRegion countBeaconRegion )
    {
        final GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);
        final GwBeacon gwBeacon = bd.selectBeacon(countBeaconRegion.getUuid().toUpperCase(),Integer.valueOf(countBeaconRegion.getMajor()), Integer.valueOf(countBeaconRegion.getMinor()));
        ScheduledExecutorService scheduled = Executors.newSingleThreadScheduledExecutor();
        if( gwBeacon != null )
        {
            final GwMessages gwNotificationExit = bd.selectNotificationExit(gwBeacon.getUid());
            long tiempo = System.currentTimeMillis();
            GwBeaconActions actionEnter = bd.selectBeaconActionsEnter(gwBeacon.getUuid().toUpperCase(), gwBeacon.getMajor(), gwBeacon.getMinor());

            if( gwNotificationExit != null )
            {
                GwNotifyTrigger trigger = bd.selectLastNotifyTrigger(gwBeacon.getUuid(), gwBeacon.getMajor(), gwBeacon.getMinor(), gwNotificationExit.getUid());
                //Comprobar que tiene relanzamiento
                if( gwNotificationExit.getRelaunchInterval() > -1 )
                {
                    //Pasar los segundos a milisegundos
                    long mSegundosNotificacion = gwNotificationExit.getRelaunchInterval() * 1000;
                    long tTranscurrido = tiempo - (trigger != null ? trigger.getTimeLaunch() : 0) ;
                    if( tTranscurrido >= mSegundosNotificacion )
                    {
                        final GwNotifyTrigger notifyTrigger = new GwNotifyTrigger(GwConstants.getSharedPreferences(context, GwConstants.UUID_COMPANY), gwNotificationExit.getUid(), gwBeacon.getMajor(), gwBeacon.getMinor(), System.currentTimeMillis());
                        //Tratar la notificacion de salida
                        if ( gwNotificationExit.isShow() )
                        {
                            if ( gwNotificationExit.getTitle() != null && gwNotificationExit.getTitle().length() > 0 )
                            {
                                if ( GwConstants.isPossibleLimitCampaingAndUser(gwNotificationExit.getCampaign()) )
                                {
                                    if ( isDayOfWeek(gwNotificationExit.getWeekdays()) && isTime(gwNotificationExit.getStartTime(), gwNotificationExit.getEndTime()) )
                                    {
                                        scheduled.schedule(new Runnable()
                                        {
                                            @Override
                                            public void run()
                                            {
                                                Log.e("Gwella", "Envio notificacion salida; " + gwBeacon.getMajorString() + " - " + gwBeacon.getMinorString());
                                                GwAssociationNotifications beaconNotifications = bd.selectAssociationNotification(gwNotificationExit.getId(), gwBeacon.getUid());
                                                String idNotify = gwBeacon.getMinorString() + beaconNotifications.getOrder();
                                                bd.insertNotifyTrigger(notifyTrigger);
                                                GwConstants.sendNotification(context, gwNotificationExit.getTitle(), gwNotificationExit.getContent(), Integer.valueOf(idNotify), gwNotificationExit.getUid(), gwBeacon.getUid(), K_SOURCE_BEACON);
                                            }
                                        }, ( long ) gwNotificationExit.getExitInterval(), TimeUnit.SECONDS);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //Hacer la salida del beacon
            exitBeaconWithoutNotify(bd, tiempo, actionEnter, gwBeacon);
        }
    }

    public static void exitBeaconWithoutNotify(GwMiAdapterBD bd, long tiempo, GwBeaconActions actionEnter, GwBeacon beacon)
    {
        if( actionEnter != null )
        {
            actionEnter.setLast_exit(tiempo);
            bd.actualizarBeaconActions(actionEnter.getId(), actionEnter);

            bd.insertRegistry(new GwRegistry(
                    0,
                    GwConstants.K_SOURCE_BEACON,
                    beacon.getUid(),
                    GwConstants.K_ACTION_EXIT,
                    "",
                    GwConstants.formatingDateToServer(),
                    false,
                    System.currentTimeMillis()
            ));
        }
    }

    //Metodo para transformar la fecha del dispositivo en el formato ISO8601 que es el del servidor
    public static String formatingDateToServer()
    {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'+0000\'");

        formatDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        String fecha = formatDate.format(c.getTime());

        return fecha;

    }

    public static String formatingDateToServerString( Date date )
    {
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'+0000\'");

        formatDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        String fecha = formatDate.format(date.getTime());

        return fecha;

    }

    //Metodo para transformar la fecha que viene del servidor en el formato ISO8601 a Date
    public static Date formatingDateToServer( String fechaServidor )
    {
        SimpleDateFormat reFormated = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:sszzz");

//        String serverDate = "2015-12-20T12:30:11+0000";

        Date date;
        try
        {
            date = reFormated.parse(fechaServidor);


        }catch ( ParseException e )
        {
            date = new Date();
        }

        return date;
    }

    public static String getRegistrationId(Context context)
    {
        SharedPreferences prefs = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);

        String registrationId = getRegistrationIdGoogle(context);

        if (registrationId.length() == 0)
        {
            Log.d(TAG, "Registro GCM no encontrado.");
            return "";
        }

        int registeredVersion =
                prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);

        long expirationTime =
                prefs.getLong(PROPERTY_EXPIRATION_TIME, -1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        String expirationDate = sdf.format(new Date(expirationTime));

        Log.d(TAG, "Registro GCM encontrado ( version=" + registeredVersion +
                ", expira=" + expirationDate + ")");

        int currentVersion = getVersionCode(context);

        if (registeredVersion != currentVersion)
        {
            Log.d(TAG, "Nueva versión de la aplicación.");
            return K_UPDATED;
        }
        else if (System.currentTimeMillis() > expirationTime)
        {
            Log.d(TAG, "Registro GCM expirado.");
            return K_UPDATED;
        }

        return registrationId;
    }

    public static void setRegistrationId(Context context, String regId)
    {
        SharedPreferences prefs = context.getSharedPreferences("Gwella", Context.MODE_PRIVATE);

        int appVersion = getVersionCode(context);

        setRegistrationIdGoogle(context, regId);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putLong(PROPERTY_EXPIRATION_TIME,
                System.currentTimeMillis() + EXPIRATION_TIME_MS);

        editor.commit();
    }

    public static String getVersion( Context context ){
        try {
            return context.getPackageManager().getPackageInfo( context.getPackageName(), 0).versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int getVersionCode( Context context ){
        try {
            return context.getPackageManager().getPackageInfo( context.getPackageName(), 0).versionCode;
        }
        catch ( PackageManager.NameNotFoundException e ) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Metodo que devuelve el deviceId unico que generamos, si no existe en la sdcard, y guardamos para
     * poder compartirlo con otras apps del mismo cliente.
     * @param context El contexto de la app para poder acceder a las SharedPreferences.
     * @return String que es device_id
     */
    public static String getDeviceId(Context context)
    {
        String deviceId;
        if ( checkSDCard() )
        {
            if ( fileSdCardExists(FILE_SDCARD_DEVICE_ID) )
            {
                //Leer el archivo y devolver el resultado, comprobando que sea un uuid valido, 36 caracteres
                deviceId = readFileExt(FILE_SDCARD_DEVICE_ID);
                if( deviceId.length() == 36 )
                {
                    //Guardar en las Shared preferences. Y devuelvo resultado.
                    GwConstants.setSharedPreferences(context, GwConstants.DEVICE_ID, deviceId);
                    return deviceId;

                }
                else
                {
                    //Genero, devuelvo y guardo resultado
                    if ( GwConstants.getSharedPreferences(context, GwConstants.DEVICE_ID).length() > 0 )
                    {
                        deviceId = GwConstants.getSharedPreferences(context, GwConstants.DEVICE_ID);
                    }
                    else
                    {
                        deviceId = generateUUID();
                    }
                    GwConstants.setSharedPreferences(context, GwConstants.DEVICE_ID, deviceId);
                    writeFileExt(FILE_SDCARD_DEVICE_ID, deviceId);
                    return deviceId;
                }
            }
            else
            {
                //Generar el uuid, crear el archivo, guardarlo en la SDcard y SharedPreferences, devolver el resultado.
                if ( GwConstants.getSharedPreferences(context, GwConstants.DEVICE_ID).length() > 0 )
                {
                    deviceId = GwConstants.getSharedPreferences(context, GwConstants.DEVICE_ID);
                }
                else
                {
                    deviceId = generateUUID();
                }
                GwConstants.setSharedPreferences(context, GwConstants.DEVICE_ID, deviceId);
                writeFileExt(FILE_SDCARD_DEVICE_ID, deviceId);
                return deviceId;
            }
        }
        else
        {
            if ( GwConstants.getSharedPreferences(context, GwConstants.DEVICE_ID).length() > 0 )
            {
                return GwConstants.getSharedPreferences(context, GwConstants.DEVICE_ID);
            }
            else
            {
                deviceId = generateUUID();
                GwConstants.setSharedPreferences(context, GwConstants.DEVICE_ID, deviceId);
                return deviceId;
            }
        }
    }

    public static void writeFileExt(String file, String texto)
    {
        try
        {
            File ruta_sd = Environment.getExternalStorageDirectory();

            File f = new File(ruta_sd.getAbsolutePath(), file);

            OutputStreamWriter fout =
                    new OutputStreamWriter(
                            new FileOutputStream(f));

            fout.write(texto);
            fout.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
        }
    }

    public static String readFileExt(String file )
    {
        String texto = "";
        try
        {
            File ruta_sd = Environment.getExternalStorageDirectory();

            File f = new File(ruta_sd.getAbsolutePath(), file);

            BufferedReader fin =
                    new BufferedReader(
                            new InputStreamReader(
                                    new FileInputStream(f)));

            texto = fin.readLine();
            fin.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde tarjeta SD");
        }
        return texto;
    }

    /**
     * Comprueba si existe el archivo donde guardamos el deviceId en la sdcard para que lo puedan utilizar las
     * distintas app de un cliente.
     * @return <b>true</b> si existe, <b>false</b> si no existe.
     */
    public static boolean fileSdCardExists( String file )
    {
        File ruta_sd = Environment.getExternalStorageDirectory();

        File f = new File(ruta_sd.getAbsolutePath(), file);

        return f.exists();
    }

    /**
     * Comprueba que esta disponible la SdCard y se puede escribir en ella.
     * @return <b>true</b> si esta disponible y se puede escribir, <b>false</b> si no esta disponible o no se puede escribir en ella.
     */
    public static boolean checkSDCard()
    {
        boolean sdDisponible;
        boolean sdAccesoEscritura;

        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String estado = Environment.getExternalStorageState();

        if (estado.equals(Environment.MEDIA_MOUNTED))
        {
            sdDisponible = true;
            sdAccesoEscritura = true;
        }
        else if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY))
        {
            sdDisponible = true;
            sdAccesoEscritura = false;
        }
        else
        {
            sdDisponible = false;
            sdAccesoEscritura = false;
        }

        return sdDisponible && sdAccesoEscritura;
    }

    /**
     * Metodo para generar un UUID aleatorio.
     * @return String que es el UUID generado.
     */
    public static String generateUUID()
    {
        return java.util.UUID.randomUUID().toString();
    }


    public static String getSecureAndroidId()
    {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public static String getAdvertaisingId()
    {
        AdvertisingIdClient.Info adver;
        try
        {
            adver = AdvertisingIdClient.getAdvertisingIdInfo(context);
            return adver.getId();

        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
        catch ( GooglePlayServicesNotAvailableException e )
        {
            e.printStackTrace();
        }
        catch ( GooglePlayServicesRepairableException e )
        {
            e.printStackTrace();
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
        return null;
    }

//    public static String generateImei()
//    {
//        String cadenaAleatoria = "";
//        long milis = new java.util.GregorianCalendar().getTimeInMillis();
//        Random r = new Random(milis);
//        int i = 0;
//        while ( i < 15){
//            char c = (char)r.nextInt(255);
//            if ( (c >= '0' && c <='9') ){
//                cadenaAleatoria += c;
//                i ++;
//            }
//        }
//        return cadenaAleatoria;
//    }

    //Metodo para que me devuelva los string de los recursos de la app
    public static String getStringRes( String clave )
    {
        String valor;
        if( clave.equals("home") )
        {
            valor = clave;
        }
        else {
            valor = context.getResources().getString(context.getResources().getIdentifier(clave, "string", context.getPackageName()));
        }

        return valor;
    }

    public static int getIntDrawableRes( String clave )
    {
        int valor = context.getResources().getIdentifier(clave, "drawable", context.getPackageName());

        return valor;
    }

    //Metodo para calcular el token de dispositivo con el imei
//    public static String getDeviceTokenImei()
//    {
//        String tokenImei = getSharedPreferences(context, PROPERTY_TOKEN_GOOGLE);
//        if( tokenImei.length() > 0 )
//        {
//            return tokenImei;
//        }
//        else
//        {
//            String imei = getDeviceId(context);
//            String cadenaAleatoria = getCadenaAlfanumAleatoria(17);
//            if( imei.length() == 15 )
//            {
//                tokenImei = cadenaAleatoria + imei;
//            }
//            else
//            {
//                imei = "G" + imei;
//                tokenImei = cadenaAleatoria + imei;
//            }
//
//            setSharedPreferences(context, PROPERTY_TOKEN_GOOGLE, tokenImei);
//
//            return tokenImei;
//        }
//    }
//
//    public static String getCadenaAlfanumAleatoria( int longitud )
//    {
//        String cadenaAleatoria = "";
//        long milis = new java.util.GregorianCalendar().getTimeInMillis();
//        Random r = new Random(milis);
//        int i = 0;
//        while ( i < longitud){
//            char c = (char)r.nextInt(255);
//            if ( (c >= '0' && c <='9') || (c >='a' && c <='z') || (c >='A' && c <='Z') ){
//                cadenaAleatoria += c;
//                i ++;
//            }
//        }
//        return cadenaAleatoria;
//    }

    public static void parseMessageIntent( Bundle extras )
    {
        if( extras != null )
        {
            if ( extras.containsKey(GwConstants.TYPE_MESSAGE) ) {
                GwConstants.insertRegistryOpen(context, extras);

                if (extras.getString(TYPE_MESSAGE).equals("push")) {
                    GwConstants.tryMessagePush(extras);
                } else {
                    if (extras.containsKey(GwConstants.MESSAGE_UID)) {
                        if (extras.getString(GwConstants.MESSAGE_UID).length() > 0) {
                            GwConstants.tryMessageBeaconAndGeo(context, extras.getString(GwConstants.MESSAGE_UID));
                        }
                    }
                }
            }
        }
    }

    public static void insertRegistryOpen(Context context, Bundle extras)
    {
        String source = "";
        String sourceId = "";
        if( extras.getString(TYPE_MESSAGE).equals("push") )
        {
            source = K_SOURCE_PUSH;
            sourceId = extras.getString("push_uid");
        }
        else if( extras.getString(TYPE_MESSAGE).equals("beacon") )
        {
            source = K_SOURCE_BEACON;
            sourceId = extras.getString(SOURCE_UID);
        }
        else if( extras.getString(TYPE_MESSAGE).equals("geo") )
        {
            source = K_SOURCE_GEO;
            sourceId = extras.getString(SOURCE_UID);
        }

        String data = "";
        if(extras.containsKey(GwConstants.MESSAGE_UID))
        {
            JSONObject j_uidNotify = new JSONObject();
            try
            {
                j_uidNotify.put("notification_uid", extras.getString(GwConstants.MESSAGE_UID));
            }catch ( JSONException e )
            {
                e.printStackTrace();
            }
            data = j_uidNotify.toString();
        }

        GwRegistry gwRegistry = new GwRegistry(0, source, sourceId, GwConstants.K_ACTION_OPEN, data, GwConstants.formatingDateToServer(), false, System.currentTimeMillis());
        GwMiAdapterBD.getInstance(context).insertRegistry(gwRegistry);
    }

    private static String getImageNotifPath(String message)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(message);

            if ( jsonObject.getString("imageNotifPath").length() > 4 )
            {
                return jsonObject.getString("imageNotifPath");
            }
        }catch ( JSONException e )
        {
            e.printStackTrace();
        }
        return null;
    }

    private static String getSound(String message)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(message);

            if ( jsonObject.getString("sound").length() > 1 )
            {
                return jsonObject.getString("sound");
            }
        }catch ( JSONException e )
        {
            e.printStackTrace();
        }
        return null;
    }



    private static void tryMessagePush( Bundle extras )
    {
        try
        {
            JSONObject jsonObject = new JSONObject(extras.getString("message"));

            if ( jsonObject.getString("imagePath").length() > 4 )
            {
                //Mostrar la imagen y al pulsar navegar a la url
                Intent intent = new Intent(context, ImageActionActivity.class);
                intent.putExtra("imagePath", jsonObject.getString("imagePath"));
                intent.putExtra("url", jsonObject.getString("url"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            else if ( jsonObject.getString("url").length() > 0 )
            {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(jsonObject.getString("url")));
                intentBrowser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentBrowser);
            }
            else if ( jsonObject.getString("view_id").length() > 0 )
            {
                //Mostrar la activity indicada
                parseAndShowActivity(jsonObject.getString("view_id"));

            }
        }catch ( JSONException e )
        {
            e.printStackTrace();
        }catch ( ClassNotFoundException e )
        {
            e.printStackTrace();
        }
    }

    public static void tryMessageBeaconAndGeo(Context applicationContext, String uidMessage)
    {
        GwMessages message = GwMiAdapterBD.getInstance(applicationContext).selectMessage(uidMessage);

        if( message != null )
        {
            if( message.getImagePath().length() > 4 )
            {
                //Mostrar la imagen y al pulsar navegar a la url
                Intent intent = new Intent(context, ImageActionActivity.class);
                intent.putExtra("imagePath", message.getImagePath());
                intent.putExtra("url", message.getUrl());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
            else if( message.getUrl().length() > 0 )
            {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(message.getUrl()));
                intentBrowser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentBrowser);
            }
            else if( message.getViewId().length() > 0 )
            {
                //Mostrar la activity indicada
                try
                {
                    parseAndShowActivity(message.getViewId());
                }catch ( JSONException e )
                {
                    e.printStackTrace();
                }catch ( ClassNotFoundException e )
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void parseAndShowActivity(final String sData) throws JSONException, ClassNotFoundException
    {
        //Desglosar el string enviado
        JSONObject jViewData = new JSONObject(sData);
        JSONObject jAndroid = jViewData.getJSONObject("android");
        String nameClass = getStringRes(jAndroid.getString("name"));

        if( !nameClass.equalsIgnoreCase("home") )
        {
            Intent intent = new Intent(context, Class.forName(nameClass));
            //Comprobar si hay que pasar parametros a la actividad
            if ( jAndroid.has("parameters") )
            {
                JSONArray jArrayData = jAndroid.getJSONArray("parameters");
                for ( int i = 0; i < jArrayData.length(); i++ )
                {
                    JSONObject jObject = jArrayData.getJSONObject(i);
                    switch ( jObject.getString("type") )
                    {
                        case "integer":
                            intent.putExtra(jObject.getString("key"), jObject.getInt("value"));
                            break;
                        case "date":
                        case "string":
                            intent.putExtra(jObject.getString("key"), jObject.getString("value"));
                            break;
                        case "boolean":
                            intent.putExtra(jObject.getString("key"), jObject.getBoolean("value"));
                            break;
                        case "float":
                            intent.putExtra(jObject.getString("key"), jObject.getDouble("value"));
                            break;
                        default:
                            intent.putExtra(jObject.getString("key"), jObject.getString("value"));
                            break;
                    }
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public static boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS)
        {
            Log.i(TAG, "Dispositivo no soportado.");
            return false;
        }
        return true;
    }

    public static void inicializeLibrary( Context context )
    {
        GwConstants.context = context;

        if ( checkPlayServices() )
        {
            //Obtenemos el Registration ID guardado
            String regid = GwConstants.getRegistrationId(context);

            //Si no disponemos de Registration ID comenzamos el registro
            if ( regid.equals("") || regid.equals(GwConstants.K_UPDATED) )
            {
                TareaRegistroGCM tarea = new TareaRegistroGCM(context);
                tarea.execute(regid);
            }
            else
            {
                GwConstants.inicializarProcesoPrincipal(context);
            }
        }
        else
        {
            Log.i(TAG, "No se ha encontrado Google Play Services.");
        }
    }

    public static boolean isPossibleLimitCampaingAndUser(final String uidCampaign)
    {
        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);

        GwCampaign campaign = bd.selectCampaign(uidCampaign);

        if( campaign != null && campaign.getMessagesCountCampaign() < campaign.getMessagesLimitCampaign() )
        {
            if( getSharedPreferencesInt(context, K_COUNT_USER + uidCampaign) < campaign.getMessagesLimitUser() )
            {
                return true;
            }
        }

        return false;
    }

    public static boolean isTime( String from, String through )
    {
        String[] parseTimeFrom = from.split(":");
        String[] parseTimeThrough = through.split(":");
        
        Calendar cFrom = Calendar.getInstance();
        cFrom.set(Calendar.HOUR_OF_DAY, Integer.valueOf(parseTimeFrom[0]));
        cFrom.set(Calendar.MINUTE, Integer.valueOf(parseTimeFrom[1]));

        Calendar cThrough = Calendar.getInstance();
        cThrough.set(Calendar.HOUR_OF_DAY, Integer.valueOf(parseTimeThrough[0]));
        cThrough.set(Calendar.MINUTE, Integer.valueOf(parseTimeThrough[1]));

        Calendar current = Calendar.getInstance();

        return ((current.get(Calendar.HOUR_OF_DAY) >= cFrom.get(Calendar.HOUR_OF_DAY)) && (current.get(Calendar.MINUTE) >= cFrom.get(Calendar.MINUTE))) && ((current.get(Calendar.HOUR_OF_DAY) <= cThrough.get(Calendar.HOUR_OF_DAY)) && (current.get(Calendar.MINUTE) <= cThrough.get(Calendar.MINUTE)));

    }

    public static boolean isDayOfWeek(String weekdays )
    {
        Calendar c = Calendar.getInstance();

        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        String day = String.valueOf(dayOfWeek);

        return weekdays.contains(day);
    }

    /**
     * Calculates the end-point from a given source at a given range (meters)
     * and bearing (degrees). This methods uses simple geometry equations to
     * calculate the end-point.
     *
     * @param point
     *            Point of origin
     * @param range
     *            Range in meters
     * @param bearing
     *            Bearing in degrees
     * @return End-point from the source given the desired range and bearing.
     */
    public static PointF calculateDerivedPosition(PointF point, double range, double bearing)
    {
        double EarthRadius = 6371000; // m

        double latA = Math.toRadians(point.x);
        double lonA = Math.toRadians(point.y);
        double angularDistance = range / EarthRadius;
        double trueCourse = Math.toRadians(bearing);

        double lat = Math.asin(
                Math.sin(latA) * Math.cos(angularDistance) +
                        Math.cos(latA) * Math.sin(angularDistance)
                                * Math.cos(trueCourse));

        double dlon = Math.atan2(
                Math.sin(trueCourse) * Math.sin(angularDistance)
                        * Math.cos(latA),
                Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat));

        double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

        lat = Math.toDegrees(lat);
        lon = Math.toDegrees(lon);

        PointF newPoint = new PointF((float) lat, (float) lon);

        return newPoint;

    }

    public static boolean pointIsInCircle(PointF pointForCheck, int radiusCheck, PointF center,
                                          double radius) {
        if (getDistanceBetweenTwoPoints(pointForCheck, radiusCheck, center) <= radius)
            return true;
        else
            return false;
    }

    public static double getDistanceBetweenTwoPoints(PointF p1, int radiusCheck, PointF p2)
    {
        double R = 6371000; // m
        double dLat = Math.toRadians(p2.x - p1.x);
        double dLon = Math.toRadians(p2.y - p1.y);
        double lat1 = Math.toRadians(p1.x);
        double lat2 = Math.toRadians(p2.x);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
                * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;

        return d - radiusCheck;
    }


}
