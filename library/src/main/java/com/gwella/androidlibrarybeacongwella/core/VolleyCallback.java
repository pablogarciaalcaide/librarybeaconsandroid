package com.gwella.androidlibrarybeacongwella.core;

/**
 * Created by juangra on 30/11/17.
 */

public interface VolleyCallback
{
    void onSuccess(String result);
    void failed(Exception e);
}
