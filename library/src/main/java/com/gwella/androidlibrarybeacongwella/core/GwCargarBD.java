package com.gwella.androidlibrarybeacongwella.core;

import android.content.Context;

/**
 * @author juangra on 3/9/15.
 */
public class GwCargarBD {

    private final String TAG = "GwCargarBD";
    private Context context;
    private GwMiAdapterBD db;

    public GwCargarBD() {}

    public GwCargarBD(Context context) {
        this.context = context;
        db = GwMiAdapterBD.getInstance(context);
    }

    public GwCargarBD( GwMiAdapterBD db, Context context ) {
        this.db = db;
        this.context = context;
    }

    public void cargarBDVolley()
    {
        HttpController.getInstance(context).sendInfo();
    }



}
