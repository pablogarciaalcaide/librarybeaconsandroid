package com.gwella.androidlibrarybeacongwella.core;

import android.util.Log;

/**
 * @author juangra on 23/9/16.
 */

public class GwEnviroment
{
    private final String TAG = "GwEnviroment";

    public static String PROPERTY_ENVIROMENT = "";
    public static String URL_DEFECT = "";

    public GwEnviroment()
    {}

    public void addEnviroment(final String enviroment,final String url_defecto)
    {
        if( enviroment.equals(GwConstants.ENVIROMENT_DEVELOPMENT) )
        {
            Log.d(TAG, "Desarrollo");
            PROPERTY_ENVIROMENT = GwConstants.ENVIROMENT_DEVELOPMENT;
            URL_DEFECT = "https://development.gwellagcm.com/api/";
        }
        else if( enviroment.equals(GwConstants.ENVIROMENT_PRODUCTION) )
        {
            Log.d(TAG, "Produccion");
            PROPERTY_ENVIROMENT = GwConstants.ENVIROMENT_PRODUCTION;
            URL_DEFECT = "https://gwellagcm.com/api/";
        }
        else if( enviroment.equals(GwConstants.ENVIROMENT_PRODUCTION) )
        {
            Log.d(TAG, "Produccion");
            PROPERTY_ENVIROMENT = GwConstants.ENVIROMENT_CUSTOM;
            URL_DEFECT = url_defecto;
        }
        else
        {
            Log.d(TAG, "Defecto");
            PROPERTY_ENVIROMENT = GwConstants.ENVIROMENT_DEVELOPMENT;
            URL_DEFECT = "https://development.gwellagcm.com/api/";
        }
    }

}
