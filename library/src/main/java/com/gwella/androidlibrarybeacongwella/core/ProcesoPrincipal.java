package com.gwella.androidlibrarybeacongwella.core;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;

import com.gwella.androidlibrarybeacongwella.model.GwCountBeaconRegion;
import com.gwella.androidlibrarybeacongwella.model.GwUserActive;
import com.gwella.androidlibrarybeacongwella.services.GwGeofencesService;
import com.gwella.androidlibrarybeacongwella.services.GwServiceMonitoring;
import com.gwella.androidlibrarybeacongwella.services.LocationService;
import com.gwella.androidlibrarybeacongwella.services.RegistrySendServer;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author juangra on 18/9/15.
 */
public class ProcesoPrincipal implements BootstrapNotifier
{
    private static final String TAG = "GwellaProcesoPrincipal";

    private static ProcesoPrincipal singleton_pp;
    public boolean inicializado = false;
    private Context context;

    BeaconManager beaconManager = null;

    public ProcesoPrincipal(){}

    public synchronized static ProcesoPrincipal getInstance(Context context)
    {
        if (singleton_pp == null) {
            singleton_pp = new ProcesoPrincipal(context);
        }
        return singleton_pp;
    }

    public ProcesoPrincipal(Context context)
    {
        this.context = context;
    }

    public synchronized void init()
    {
        //Borramos el archivo de control de los mensajes push
        GwConstants.clearSharedPreferences(context, GwConstants.NAME_FILE_CONTROL_PUSH);


        //Sirve para borrar todos los datos de la bd y la carga con los datos predefinidos
        GwCargarBD cargarBD = new GwCargarBD(context);


        //Creo una instancia de la base bbdd y compruebo que hay usuario activo
        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
        GwUserActive userActive = bd.selectUserActive();
//        if( userActive == null || GwConstants.sendActualizeTokenGoogle )
        if( userActive == null )
        {
            //Si no existe hacemos una peticion de installacion
            HttpController.getInstance(context).sendInstallation();
        }
        else
        {
            //Si existe hacemos la peticion de la carga de datos
            Log.i( TAG, "Ya estamos registrados");
            cargarBD.cargarBDVolley();
        }


        //Create instance of the beacon manager
        beaconManager = BeaconManager.getInstanceForApplication(context);

        beaconManager.setBackgroundMode(true);
        beaconManager.setForegroundScanPeriod(1000);
        beaconManager.setForegroundBetweenScanPeriod(2000);
        beaconManager.setBackgroundScanPeriod(1000);
        beaconManager.setBackgroundBetweenScanPeriod(3000);

        // By default the AndroidBeaconLibrary will only find AltBeacons.  If you wish to make it
        // find a different type of beacon, you must specify the byte layout for that beacon's
        // advertisement with a line like below.  The example shows how to find a beacon with the
        // same byte layout as AltBeacon but with a beaconTypeCode of 0xaabb.  To find the proper
        // layout expression for other beacon types, do a web search for "setBeaconLayout"
        // including the quotes.


//         beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=aabb,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        //Arranco un timer para que le de tiempo a la base de datos crearse.
        Timer myTimer = new Timer();
        myTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {

                if ( !inicializado ) inicializarBeacons();
            }

        }, 3000);

        new BackgroundPowerSaver(context);

        if( GwConstants.LOCATION_ACTIVE )
        {
            //Iniciamos el locationService
            new LocationService(context);
        }

        if( GwConstants.GEOFENCING_ACTIVE )
        {

            //Arranco un timer para que le de tiempo a la base de datos crearse.
            Timer myTimer2 = new Timer();
            myTimer2.schedule(new TimerTask()
            {
                @Override
                public void run()
                {
                    if( GwConstants.GEOFENCING_ACTIVE )
                    {
                        Intent intentService = new Intent(context, GwGeofencesService.class);
                        context.startService(intentService);
                    }
                }

            }, 3000);

        }

        //Iniciamos el envio de los registros
        new RegistrySendServer(context);
    }

    public void inicializarBeacons() {

        if( GwConstants.BEACONS_ACTIVE )
        {
            inicializado = true;
            Region region = new Region(GwConstants.getSharedPreferences(context, GwConstants.IDENTIFIER_COMPANY), Identifier.parse(GwConstants.getSharedPreferences(context, GwConstants.UUID_COMPANY)), null, null);
            new RegionBootstrap(this, region);
        }
    }


    @Override
    public Context getApplicationContext()
    {
        return context;
    }

    @Override
    public void didEnterRegion(Region region)
    {
//        Log.i(TAG, "1 Enter Region");
    }

    @Override
    public void didExitRegion(Region region)
    {
//        Log.i(TAG, "1 Exit Region" );
    }

    @Override
    public void didDetermineStateForRegion(int i, Region region)
    {
        Log.i(TAG, "1 Determine state Region: " + i);
        switch (i)
        {
            case MonitorNotifier.INSIDE:
                try {
                    beaconManager.startRangingBeaconsInRegion(region);
//                    beaconManager.startMonitoringBeaconsInRegion(region);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case MonitorNotifier.OUTSIDE:
                try {

                    //Todo: esto es temporal hasta que encuentre una solucion optima
                    //Comprobar cual ha sido el ultimo beacon y realizar las comprobaciones de salida
                    beaconManager.stopRangingBeaconsInRegion(region);
                    //TODO: comprobar si existe algun beacon en el array y proceder a registrar su salida, antes de borrar el array
                    for( GwCountBeaconRegion countBeaconRegion : GwConstants.countBeaconRegions )
                    {
                        GwConstants.exitBeacon(countBeaconRegion);
                    }
                    GwConstants.countBeaconRegions.clear();
//                    beaconManager.stopMonitoringBeaconsInRegion(region);
                    //Borro los enter para que cuando vuelva a entrar me vuelva a notificar.
//                    GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
//                    bd.deleteBeacon_actionsEnter(region.getId1().toString());

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
