package com.gwella.androidlibrarybeacongwella.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.gwella.androidlibrarybeacongwella.model.GwBeacon;
import com.gwella.androidlibrarybeacongwella.model.GwBeaconActions;
import com.gwella.androidlibrarybeacongwella.model.GwCampaign;
import com.gwella.androidlibrarybeacongwella.model.GwGeofence;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;
import com.gwella.androidlibrarybeacongwella.model.GwAssociationNotifications;
import com.gwella.androidlibrarybeacongwella.model.GwMessages;
import com.gwella.androidlibrarybeacongwella.model.GwNotifyTrigger;
import com.gwella.androidlibrarybeacongwella.model.GwRules;
import com.gwella.androidlibrarybeacongwella.model.GwUserActive;

import java.util.ArrayList;

/**
 * @author juangra on 2/9/15.
 */

public class GwMiAdapterBD {

    private static GwMiAdapterBD bd_singleton;

    private static final int VERSION_BASE_DATOS = 2;

    private Context context;
    private SQLiteDatabase db;
    private static String TAG = "Gwella";

    public static final String TABLA_BEACON_ACTIONS = "beacon_actions";
    public static final String TABLA_BEACON = "beacon";
    public static final String TABLA_ASSOCIATION_NOTIFICATION = "association_notification";
    public static final String TABLA_MESSAGES = "messages";
    public static final String TABLA_NOTIFY_TRIGGER = "notify_trigger";
    public static final String TABLA_REGISTRY = "registry";
    public static final String TABLA_USER_ACTIVE = "user_active";
    public static final String TABLA_GEOFENCE = "geofence";
    public static final String TABLA_RULES = "rules";
    public static final String TABLA_CAMPAIGN = "campaign";

    //************************************************************************
    // NOMBRE COLUMNAS
    private static final String[] COLUMNAS_BEACON_ACTIONS = {"id", "uuid", "major", "minor", "last_exit", "last_enter", "last_range_notify"};

    private static final String[] COLUMNAS_BEACON = {"uuid", "major", "minor", "key", "monitoring"};

    private static final String[] COLUMNAS_ASSOCIATION_NOTIFICATION = {"key_event", "key_notification", "order_notify"};

    private static final String[] COLUMNAS_MESSAGES = { "id", "uid", "show", "title", "content", "enter_interval", "exit_interval", "distance", "relaunch_interval", "view_id", "url", "image_path", "campaign", "weekdays", "start_time", "end_time", "image_notif_path", "sound" };

    private static final String[] COLUMNAS_NOTIFY_TRIGGER = {"uuid", "key_notification", "major", "minor", "time_launch" };

    private static final String[] COLUMNAS_REGISTRY = {"id", "source", "uid", "action", "data", "date_created", "sincronizado", "date_created_millis" };

    private static final String[] COLUMNAS_USER_ACTIVE = {"id", "client_token", "device_token", "app_name", "app_version", "device_type" };

    private static final String[] COLUMNAS_GEOFENCE = {"id", "uid", "type", "latitude", "longitude", "radius" };

    private static final String[] COLUMNAS_RULES = {"_id", "uid_chanel", "campaign", "distance", "enter_interval", "exit_interval", "relaunch_interval", "uid_message", "weekdays", "start_time", "end_time"};

    private static final String[] COLUMNAS_CAMPAIGN = {"id", "uid", "name", "start_date", "end_date", "messages_limit_campaign", "messages_count_campaign", "messages_limit_user" };

    //************************************************************************
    // NOMBRE INSERTS
    private SQLiteStatement insertStatementBeaconActions;
    private SQLiteStatement insertStatementBeacon;
    private SQLiteStatement insertStatementAssociationNotification;
    private SQLiteStatement insertStatementMessages;
    private SQLiteStatement insertStatementNotifyTrigger;
    private SQLiteStatement insertStatementRegistry;
    private SQLiteStatement insertStatementUserActive;
    private SQLiteStatement insertStatementGeofence;
    private SQLiteStatement insertStatementRules;
    private SQLiteStatement insertStatementCampaign;

    private static final String INSERT_BEACON_ACTIONS = "insert into " + TABLA_BEACON_ACTIONS + "(" +
            COLUMNAS_BEACON_ACTIONS[1] + ", " + COLUMNAS_BEACON_ACTIONS[2] + ", " + COLUMNAS_BEACON_ACTIONS[3] + ", " +
            COLUMNAS_BEACON_ACTIONS[4] + ", " + COLUMNAS_BEACON_ACTIONS[5] + ", " + COLUMNAS_BEACON_ACTIONS[6] + " " +
            ") values ( ?, ?, ?, ?, ?, ?)";

    private static final String INSERT_BEACON = "insert into " + TABLA_BEACON + "(" +
            COLUMNAS_BEACON[0] + ", " + COLUMNAS_BEACON[1] + ", " + COLUMNAS_BEACON[2] + ", " + COLUMNAS_BEACON[3] + ", " + COLUMNAS_BEACON[4] +
            ") values ( ?, ?, ?, ?, ? )";

    private static final String INSERT_ASSOCIATION_NOTIFICATION = "insert into " + TABLA_ASSOCIATION_NOTIFICATION + "(" +
            COLUMNAS_ASSOCIATION_NOTIFICATION[0] + ", " + COLUMNAS_ASSOCIATION_NOTIFICATION[1] + ", " + COLUMNAS_ASSOCIATION_NOTIFICATION[2] +
            ") values ( ?, ?, ? )";

    private static final String INSERT_MESSAGES = "insert into " + TABLA_MESSAGES + "(" +
            COLUMNAS_MESSAGES[1] + ", " + COLUMNAS_MESSAGES[2] + ", " + COLUMNAS_MESSAGES[3] + ", " +
            COLUMNAS_MESSAGES[4] + ", " + COLUMNAS_MESSAGES[5] + ", " + COLUMNAS_MESSAGES[6] + ", " +
            COLUMNAS_MESSAGES[7] + ", " + COLUMNAS_MESSAGES[8] + ", " + COLUMNAS_MESSAGES[9] + ", " +
            COLUMNAS_MESSAGES[10] + ", " + COLUMNAS_MESSAGES[11] + ", " + COLUMNAS_MESSAGES[12] + ", " +
            COLUMNAS_MESSAGES[13] + ", " + COLUMNAS_MESSAGES[14] + ", " + COLUMNAS_MESSAGES[15] + ", " +
            COLUMNAS_MESSAGES[16] + ", " + COLUMNAS_MESSAGES[17] + ") values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

    private static final String INSERT_NOTIFY_TRIGGER = "insert into " + TABLA_NOTIFY_TRIGGER + "(" +
            COLUMNAS_NOTIFY_TRIGGER[0] + ", " + COLUMNAS_NOTIFY_TRIGGER[1] + ", " + COLUMNAS_NOTIFY_TRIGGER[2] + ", " +
            COLUMNAS_NOTIFY_TRIGGER[3] + ", " + COLUMNAS_NOTIFY_TRIGGER[4] +
            ") values ( ?, ?, ?, ?, ? )";

    private static final String INSERT_REGISTRY = "insert into " + TABLA_REGISTRY + "(" +
            COLUMNAS_REGISTRY[1] + ", " + COLUMNAS_REGISTRY[2] + ", " + COLUMNAS_REGISTRY[3] + ", " + COLUMNAS_REGISTRY[4] + ", " +
            COLUMNAS_REGISTRY[5] + ", " + COLUMNAS_REGISTRY[6] + ", " + COLUMNAS_REGISTRY[7] +
            ") values ( ?, ?, ?, ?, ?, ?, ? )";

    private static final String INSERT_USER_ACTIVE = "insert into " + TABLA_USER_ACTIVE + "(" +
            COLUMNAS_USER_ACTIVE[1] + ", " + COLUMNAS_USER_ACTIVE[2] + ", " + COLUMNAS_USER_ACTIVE[3] + ", " +
            COLUMNAS_USER_ACTIVE[4] + ", " + COLUMNAS_USER_ACTIVE[5] +
            ") values ( ?, ?, ?, ?, ? )";

    private static final String INSERT_GEOFENCE = "insert into " + TABLA_GEOFENCE + "(" +
            COLUMNAS_GEOFENCE[1] + ", " + COLUMNAS_GEOFENCE[2] + ", " + COLUMNAS_GEOFENCE[3] + ", " + COLUMNAS_GEOFENCE[4] + ", " + COLUMNAS_GEOFENCE[5] +
            ") values ( ?, ?, ?, ?, ? )";

    private static final String INSERT_RULE = "insert into " + TABLA_RULES + "(" +
            COLUMNAS_RULES[1] + ", " + COLUMNAS_RULES[2] + ", " + COLUMNAS_RULES[3] +
            ", " + COLUMNAS_RULES[4] + ", " + COLUMNAS_RULES[5] + ", " + COLUMNAS_RULES[6] +
            ", " + COLUMNAS_RULES[7] + ", " + COLUMNAS_RULES[8] + ", " + COLUMNAS_RULES[9] +
            ", " + COLUMNAS_RULES[10] + ") values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

    private static final String INSERT_CAMPAIGN = "insert into " + TABLA_CAMPAIGN + "(" +
            COLUMNAS_CAMPAIGN[1] + ", " + COLUMNAS_CAMPAIGN[2] + ", " + COLUMNAS_CAMPAIGN[3] + ", " +
            COLUMNAS_CAMPAIGN[4] + ", " + COLUMNAS_CAMPAIGN[5] + ", " + COLUMNAS_CAMPAIGN[6] + ", " +
            COLUMNAS_CAMPAIGN[7] + ") values ( ?, ?, ?, ?, ?, ?, ? )";

    //************************************************************************
    // CREATE
    private static final String CREATE_DB_BEACON_ACTIONS = "CREATE TABLE " + TABLA_BEACON_ACTIONS + "(" +
            COLUMNAS_BEACON_ACTIONS[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_BEACON_ACTIONS[1] + " VARCHAR, " +
            COLUMNAS_BEACON_ACTIONS[2] + " INTEGER, " +
            COLUMNAS_BEACON_ACTIONS[3] + " INTEGER, " +
            COLUMNAS_BEACON_ACTIONS[4] + " FLOAT, " +
            COLUMNAS_BEACON_ACTIONS[5] + " FLOAT, " +
            COLUMNAS_BEACON_ACTIONS[6] + " FLOAT ); ";

    private static final String CREATE_DB_BEACON = "CREATE TABLE " + TABLA_BEACON + "(" +
            COLUMNAS_BEACON[0] + " VARCHAR, " +
            COLUMNAS_BEACON[1] + " INTEGER, " +
            COLUMNAS_BEACON[2] + " INTEGER, " +
            COLUMNAS_BEACON[3] + " VARCHAR, " +
            COLUMNAS_BEACON[4] + " BOOLEAN); ";

    private static final String CREATE_DB_ASSOCIATION_NOTIFICATION = "CREATE TABLE " + TABLA_ASSOCIATION_NOTIFICATION + "(" +
            COLUMNAS_ASSOCIATION_NOTIFICATION[0] + " VARCHAR, " +
            COLUMNAS_ASSOCIATION_NOTIFICATION[1] + " INTEGER, " +
            COLUMNAS_ASSOCIATION_NOTIFICATION[2] + " INTEGER); ";

    private static final String CREATE_DB_MESSAGES = "CREATE TABLE " + TABLA_MESSAGES + "(" +
            COLUMNAS_MESSAGES[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_MESSAGES[1] + " VARCHAR, " +
            COLUMNAS_MESSAGES[2] + " BOOLEAN, " +
            COLUMNAS_MESSAGES[3] + " VARCHAR, " +
            COLUMNAS_MESSAGES[4] + " VARCHAR, " +
            COLUMNAS_MESSAGES[5] + " INTEGER, " +
            COLUMNAS_MESSAGES[6] + " INTEGER, " +
            COLUMNAS_MESSAGES[7] + " INTEGER, " +
            COLUMNAS_MESSAGES[8] + " INTEGER, " +
            COLUMNAS_MESSAGES[9] + " VARCHAR, " +
            COLUMNAS_MESSAGES[10] + " VARCHAR, " +
            COLUMNAS_MESSAGES[11] + " VARCHAR, " +
            COLUMNAS_MESSAGES[12] + " VARCHAR, " +
            COLUMNAS_MESSAGES[13] + " VARCHAR, " +
            COLUMNAS_MESSAGES[14] + " VARCHAR, " +
            COLUMNAS_MESSAGES[15] + " VARCHAR, " +
            COLUMNAS_MESSAGES[16] + " VARCHAR, " +
            COLUMNAS_MESSAGES[17] + " VARCHAR); ";

    private static final String CREATE_DB_NOTIFY_TRIGGER = "CREATE TABLE " + TABLA_NOTIFY_TRIGGER + "(" +
            COLUMNAS_NOTIFY_TRIGGER[0] + " VARCHAR, " +
            COLUMNAS_NOTIFY_TRIGGER[1] + " VARCHAR, " +
            COLUMNAS_NOTIFY_TRIGGER[2] + " INTEGER, " +
            COLUMNAS_NOTIFY_TRIGGER[3] + " INTEGER, " +
            COLUMNAS_NOTIFY_TRIGGER[4] + " FLOAT); ";

    private static final String CREATE_DB_REGISTRY = "CREATE TABLE " + TABLA_REGISTRY + "(" +
            COLUMNAS_REGISTRY[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_REGISTRY[1] + " VARCHAR, " +
            COLUMNAS_REGISTRY[2] + " VARCHAR, " +
            COLUMNAS_REGISTRY[3] + " VARCHAR, " +
            COLUMNAS_REGISTRY[4] + " VARCHAR, " +
            COLUMNAS_REGISTRY[5] + " VARCHAR, " +
            COLUMNAS_REGISTRY[6] + " BOOLEAN, " +
            COLUMNAS_REGISTRY[7] + " DOUBLE); ";

    private static final String CREATE_DB_USER_ACTIVE = "CREATE TABLE " + TABLA_USER_ACTIVE + "(" +
            COLUMNAS_USER_ACTIVE[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_USER_ACTIVE[1] + " VARCHAR, " +
            COLUMNAS_USER_ACTIVE[2] + " VARCHAR, " +
            COLUMNAS_USER_ACTIVE[3] + " VARCHAR, " +
            COLUMNAS_USER_ACTIVE[4] + " VARCHAR, " +
            COLUMNAS_USER_ACTIVE[5] + " VARCHAR); ";

    private static final String CREATE_DB_GEOFENCE = "CREATE TABLE " + TABLA_GEOFENCE + "(" +
            COLUMNAS_GEOFENCE[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_GEOFENCE[1] + " VARCHAR, " +
            COLUMNAS_GEOFENCE[2] + " VARCHAR, " +
            COLUMNAS_GEOFENCE[3] + " FLOAT, " +
            COLUMNAS_GEOFENCE[4] + " FLOAT, " +
            COLUMNAS_GEOFENCE[5] + " INTEGER); ";

    private static final String CREATE_DB_RULE = "CREATE TABLE " + TABLA_RULES + "(" +
            COLUMNAS_RULES[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_RULES[1] + " VARCHAR, " +
            COLUMNAS_RULES[2] + " VARCHAR, " +
            COLUMNAS_RULES[3] + " INTEGER, " +
            COLUMNAS_RULES[4] + " INTEGER, " +
            COLUMNAS_RULES[5] + " INTEGER, " +
            COLUMNAS_RULES[6] + " INTEGER, " +
            COLUMNAS_RULES[7] + " VARCHAR, " +
            COLUMNAS_RULES[8] + " VARCHAR, " +
            COLUMNAS_RULES[9] + " VARCHAR, " +
            COLUMNAS_RULES[10] + " VARCHAR); ";

    private static final String CREATE_DB_CAMPAIGN = "CREATE TABLE " + TABLA_CAMPAIGN + "(" +
            COLUMNAS_CAMPAIGN[0] + " INTEGER PRIMARY KEY, " +
            COLUMNAS_CAMPAIGN[1] + " VARCHAR, " +
            COLUMNAS_CAMPAIGN[2] + " VARCHAR, " +
            COLUMNAS_CAMPAIGN[3] + " FLOAT, " +
            COLUMNAS_CAMPAIGN[4] + " FLOAT, " +
            COLUMNAS_CAMPAIGN[5] + " INTEGER, " +
            COLUMNAS_CAMPAIGN[6] + " INTEGER, " +
            COLUMNAS_CAMPAIGN[7] + " INTEGER); ";

    private static final String CREATE_DB = CREATE_DB_BEACON_ACTIONS + CREATE_DB_BEACON +
            CREATE_DB_ASSOCIATION_NOTIFICATION + CREATE_DB_MESSAGES + CREATE_DB_NOTIFY_TRIGGER +
            CREATE_DB_REGISTRY + CREATE_DB_USER_ACTIVE + CREATE_DB_GEOFENCE + CREATE_DB_RULE + CREATE_DB_CAMPAIGN;

    //************************************************************************
    // CONSTRUCTOR
    public synchronized static GwMiAdapterBD getInstance(Context context) {
        if (bd_singleton == null) {
            bd_singleton = new GwMiAdapterBD(context.getApplicationContext());
        }
        return bd_singleton;
    }


    public GwMiAdapterBD(Context context) {

        //Iniciar el helper con el nombre de la base de datos y la versión
        this.context = context;
        MiOpenHelper openHelper = new MiOpenHelper(context, GwConfig.DB_NAME, null, VERSION_BASE_DATOS);

        try {
            openHelper.createDataBase();
        } catch (Exception ex) {
            Log.d( TAG, "Create Data Base ERROR");
            ex.printStackTrace();
        }

        try {
            db = openHelper.openDataBase();
        } catch (SQLException ex) {
            Log.e( TAG, "Open Data Base ERROR");
            ex.printStackTrace();
        }


        //Si no existe la tabla, la crea
        if (db != null) {
            if (!GwConstants.existeTabla(db, TABLA_BEACON_ACTIONS)) {
                db.execSQL(CREATE_DB_BEACON_ACTIONS);
            }

            if (!GwConstants.existeTabla(db, TABLA_BEACON)) {
                db.execSQL(CREATE_DB_BEACON);
            }

            if (!GwConstants.existeTabla(db, TABLA_ASSOCIATION_NOTIFICATION)) {
                db.execSQL(CREATE_DB_ASSOCIATION_NOTIFICATION);
            }

            if (!GwConstants.existeTabla(db, TABLA_MESSAGES)) {
                db.execSQL(CREATE_DB_MESSAGES);
            }

            if (!GwConstants.existeTabla(db, TABLA_NOTIFY_TRIGGER)) {
                db.execSQL(CREATE_DB_NOTIFY_TRIGGER);
            }

            if (!GwConstants.existeTabla(db, TABLA_REGISTRY)) {
                db.execSQL(CREATE_DB_REGISTRY);
            }

            if (!GwConstants.existeTabla(db, TABLA_USER_ACTIVE)) {
                db.execSQL(CREATE_DB_USER_ACTIVE);
            }

            if (!GwConstants.existeTabla(db, TABLA_GEOFENCE)) {
                db.execSQL(CREATE_DB_GEOFENCE);
            }
            if (!GwConstants.existeTabla(db, TABLA_RULES)) {
                db.execSQL(CREATE_DB_RULE);
            }
            if (!GwConstants.existeTabla(db, TABLA_CAMPAIGN)) {
                db.execSQL(CREATE_DB_CAMPAIGN);
            }

        }

        try {
            //Compilar la query INSERT para después usuarla en el método de insertar
            this.insertStatementBeaconActions = this.db.compileStatement(INSERT_BEACON_ACTIONS);
            this.insertStatementBeacon = this.db.compileStatement(INSERT_BEACON);
            this.insertStatementAssociationNotification = this.db.compileStatement(INSERT_ASSOCIATION_NOTIFICATION);
            this.insertStatementMessages = this.db.compileStatement(INSERT_MESSAGES);
            this.insertStatementNotifyTrigger = this.db.compileStatement(INSERT_NOTIFY_TRIGGER);
            this.insertStatementRegistry = this.db.compileStatement(INSERT_REGISTRY);
            this.insertStatementUserActive = this.db.compileStatement(INSERT_USER_ACTIVE);
            this.insertStatementGeofence = this.db.compileStatement(INSERT_GEOFENCE);
            this.insertStatementRules = this.db.compileStatement(INSERT_RULE);
            this.insertStatementCampaign = this.db.compileStatement(INSERT_CAMPAIGN);

        } catch (Exception e) {
            Log.e("Error", e.toString());
        }


    }

    //JGCB: empieza transacción
    public void beginTransaction() {
        db.beginTransaction();
    }

    //JGCB: termina transacción
    public void endTransaction() {
        db.endTransaction();
    }

    //JGCB: Commit de los cambios
    public void setTransactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public void reconstruirBD() {
        deleteAllBeacon_actions();
        deleteAllBeacon();
        deleteAllAssociationNotification();
        deleteAllMessages();
        deleteAllNotifyTrigger();
        deleteAllRegistries();
        deleteAllUserActive();
        deleteAllGeofence();
        deleteAllRules();
        deleteAllCampaign();
    }

    /*
        *
        * INSERTS
        *
        */
    public long insertBeacon_action(GwBeaconActions beaconActions) {
        this.insertStatementBeaconActions.bindString(1, beaconActions.getUUID());
        this.insertStatementBeaconActions.bindDouble(2, beaconActions.getMajor());
        this.insertStatementBeaconActions.bindDouble(3, beaconActions.getMinor());
        this.insertStatementBeaconActions.bindDouble(4, beaconActions.getLast_exit());
        this.insertStatementBeaconActions.bindDouble(5, beaconActions.getLast_enter());
        this.insertStatementBeaconActions.bindDouble(6, beaconActions.getLast_range_notify());

        return this.insertStatementBeaconActions.executeInsert();
    }

    public long insertBeacon(GwBeacon beacon) {
        this.insertStatementBeacon.bindString(1, beacon.getUuid());
        this.insertStatementBeacon.bindDouble(2, beacon.getMajor());
        this.insertStatementBeacon.bindDouble(3, beacon.getMinor());
        this.insertStatementBeacon.bindString(4, beacon.getUid());
        this.insertStatementBeacon.bindDouble(5, beacon.isMonitoring() ? 1 : 0);

        return this.insertStatementBeacon.executeInsert();
    }

    public long insertAssociationNotification(GwAssociationNotifications associationNotifications) {
        this.insertStatementAssociationNotification.bindString(1, associationNotifications.getKeyEvent());
        this.insertStatementAssociationNotification.bindDouble(2, associationNotifications.getKeyNotification());
        this.insertStatementAssociationNotification.bindDouble(3, associationNotifications.getOrder());

        return this.insertStatementAssociationNotification.executeInsert();
    }

    public long insertMessages(GwMessages gwMessages) {
        this.insertStatementMessages.bindString(1, gwMessages.getUid());
        this.insertStatementMessages.bindDouble(2, gwMessages.isShow() ? 1 : 0);
        this.insertStatementMessages.bindString(3, gwMessages.getTitle() != null ? gwMessages.getTitle() : "");
        this.insertStatementMessages.bindString(4, gwMessages.getContent() != null ? gwMessages.getContent() : "");
        this.insertStatementMessages.bindDouble(5, gwMessages.getEnterInterval());
        this.insertStatementMessages.bindDouble(6, gwMessages.getExitInterval());
        this.insertStatementMessages.bindDouble(7, gwMessages.getDistance());
        this.insertStatementMessages.bindDouble(8, gwMessages.getRelaunchInterval());
        this.insertStatementMessages.bindString(9, gwMessages.getViewId());
        this.insertStatementMessages.bindString(10, gwMessages.getUrl());
        this.insertStatementMessages.bindString(11, gwMessages.getImagePath());
        this.insertStatementMessages.bindString(12, gwMessages.getCampaign());
        this.insertStatementMessages.bindString(13, gwMessages.getWeekdays());
        this.insertStatementMessages.bindString(14, gwMessages.getStartTime());
        this.insertStatementMessages.bindString(15, gwMessages.getEndTime());
        this.insertStatementMessages.bindString(16, gwMessages.getImageNotifPath());
        this.insertStatementMessages.bindString(17, gwMessages.getSound());

        return this.insertStatementMessages.executeInsert();
    }

    public long insertNotifyTrigger(GwNotifyTrigger notifyTrigger) {
        this.insertStatementNotifyTrigger.bindString(1, notifyTrigger.getUuid());
        this.insertStatementNotifyTrigger.bindString(2, notifyTrigger.getKeyNotification());
        this.insertStatementNotifyTrigger.bindDouble(3, notifyTrigger.getMajor());
        this.insertStatementNotifyTrigger.bindDouble(4, notifyTrigger.getMinor());
        this.insertStatementNotifyTrigger.bindDouble(5, notifyTrigger.getTimeLaunch());

        return this.insertStatementNotifyTrigger.executeInsert();
    }

    public long insertRegistry(GwRegistry registry) {
        this.insertStatementRegistry.bindString(1, registry.getSource());
        this.insertStatementRegistry.bindString(2, registry.getUid());
        this.insertStatementRegistry.bindString(3, registry.getAction());
        this.insertStatementRegistry.bindString(4, registry.getData());
        this.insertStatementRegistry.bindString(5, registry.getDateCreated());
        this.insertStatementRegistry.bindDouble(6, registry.isSinchronizado() ? 1 : 0);
        this.insertStatementRegistry.bindDouble(7, registry.getSynchronDate());

        return this.insertStatementRegistry.executeInsert();
    }

    public long insertUserActive(GwUserActive userActive) {
        this.insertStatementUserActive.bindString(1, userActive.getClient_token());
        this.insertStatementUserActive.bindString(2, userActive.getDevice_token());
        this.insertStatementUserActive.bindString(3, userActive.getApp_name());
        this.insertStatementUserActive.bindString(4, userActive.getApp_version());
        this.insertStatementUserActive.bindString(5, userActive.getDevice_type());

        return this.insertStatementUserActive.executeInsert();
    }

    public long insertGeofence(GwGeofence geofence) {
        this.insertStatementGeofence.bindString(1, geofence.getUid());
        this.insertStatementGeofence.bindString(2, geofence.getType());
        this.insertStatementGeofence.bindDouble(3, geofence.getLatitude());
        this.insertStatementGeofence.bindDouble(4, geofence.getLongitude());
        this.insertStatementGeofence.bindDouble(5, geofence.getRadius());

        return this.insertStatementGeofence.executeInsert();
    }

    public long insertRule(GwRules gwRules) {
        this.insertStatementRules.bindString(1, gwRules.getUidChanel());
        this.insertStatementRules.bindString(2, gwRules.getCampaign());
        this.insertStatementRules.bindDouble(3, gwRules.getDistance());
        this.insertStatementRules.bindDouble(4, gwRules.getEnterInterval());
        this.insertStatementRules.bindDouble(5, gwRules.getExitInterval());
        this.insertStatementRules.bindDouble(6, gwRules.getRelaunchInterval());
        this.insertStatementRules.bindString(7, gwRules.getUidMessage());
        this.insertStatementRules.bindString(8, gwRules.getWeekdays());
        this.insertStatementRules.bindString(9, gwRules.getStartTime());
        this.insertStatementRules.bindString(10, gwRules.getEndTime());

        return this.insertStatementRules.executeInsert();
    }

    public long insertCampaign(GwCampaign campaign) {
        this.insertStatementCampaign.bindString(1, campaign.getUid());
        this.insertStatementCampaign.bindString(2, campaign.getName());
        this.insertStatementCampaign.bindDouble(3, campaign.getStartDate());
        this.insertStatementCampaign.bindDouble(4, campaign.getEnDate());
        this.insertStatementCampaign.bindDouble(5, campaign.getMessagesLimitCampaign());
        this.insertStatementCampaign.bindDouble(6, campaign.getMessagesCountCampaign());
        this.insertStatementCampaign.bindDouble(7, campaign.getMessagesLimitUser());

        return this.insertStatementCampaign.executeInsert();
    }

    //************************************************************************
    // DELETE ALL
    public int deleteAllBeacon_actions() {
        return db.delete(TABLA_BEACON_ACTIONS, null, null);
    }

    public int deleteBeacon_actionsEnter( String uuid ) {
        return db.delete(TABLA_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS[1] + " = '" + uuid + "' AND " + COLUMNAS_BEACON_ACTIONS[5] + " > 0 ", null);
    }

    public int deleteAllBeacon() {
        return db.delete(TABLA_BEACON, null, null);
    }

    public int deleteAllAssociationNotification() {
        return db.delete(TABLA_ASSOCIATION_NOTIFICATION, null, null);
    }

    public int deleteAllMessages() {
        return db.delete(TABLA_MESSAGES, null, null);
    }

    public int deleteAllNotifyTrigger() {
        return db.delete(TABLA_NOTIFY_TRIGGER, null, null);
    }

    public int deleteAllRegistries() {
        return db.delete(TABLA_REGISTRY, null, null);
    }

    public int deleteAllUserActive() {
        return db.delete(TABLA_USER_ACTIVE, null, null);
    }

    public int deleteAllGeofence() {
        return db.delete(TABLA_GEOFENCE, null, null);
    }

    public int deleteAllRules() {
        return db.delete(TABLA_RULES, null, null);
    }

    public int deleteAllCampaign() {
        return db.delete(TABLA_CAMPAIGN, null, null);
    }

    public int deleteRegistriesSincronized()
    {
        return db.delete(TABLA_REGISTRY, COLUMNAS_REGISTRY[6] + " = 1 ", null);
    }

    //************************************************************************
    //SELECT ALL

    public GwBeacon selectBeacon( String uuid, int major, int minor ) {

        Cursor rs = db.query(TABLA_BEACON, COLUMNAS_BEACON, COLUMNAS_BEACON[0] + " = '" + uuid +
                "' AND " + COLUMNAS_BEACON[1] + " = " + major +
                " AND " + COLUMNAS_BEACON[2] + " = " + minor, null, null, null, null);
        GwBeacon beaconInfo = null;

        try {
            if (rs.moveToFirst()) {
                beaconInfo = getFromCursorGwBeacon(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return beaconInfo;
    }

    public GwBeacon selectBeaconMajorAndMinor( int major, int minor )
    {
        Cursor rs = db.query(TABLA_BEACON, COLUMNAS_BEACON, COLUMNAS_BEACON[1] + " = " + major +
                " AND " + COLUMNAS_BEACON[2] + " = " + minor, null, null, null, null);
        GwBeacon beaconInfo = null;

        try {
            if (rs.moveToFirst()) {
                beaconInfo = getFromCursorGwBeacon(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return beaconInfo;
    }

    public ArrayList<GwBeacon> selectBeacons(String uuid) {

        Cursor rs = db.query(TABLA_BEACON, COLUMNAS_BEACON, COLUMNAS_BEACON[0] + " = '" + uuid + "' ", null, null, null, null);

        ArrayList<GwBeacon> gwBeacons = new ArrayList<GwBeacon>();

        try {
            if (rs.moveToFirst()) {
                do {
                    gwBeacons.add(getFromCursorGwBeacon(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return gwBeacons;
    }

    public ArrayList<GwBeacon> selectBeacons(String uuid, int major) {

        Cursor rs = db.query(TABLA_BEACON, COLUMNAS_BEACON, COLUMNAS_BEACON[0] + " = '" + uuid + "' AND " +
                COLUMNAS_BEACON[2] + " = " + major, null, null, null, null);

        ArrayList<GwBeacon> gwBeacons = new ArrayList<GwBeacon>();

        try {
            if (rs.moveToFirst()) {
                do {
                    gwBeacons.add(getFromCursorGwBeacon(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return gwBeacons;
    }

    public GwBeacon getFromCursorGwBeacon(Cursor rs) {
        return new GwBeacon(
                rs.getString(0),
                rs.getInt(1),
                rs.getInt(2),
                rs.getString(3),
                rs.getInt(4) == 1);
    }

    public GwBeaconActions selectBeaconActions( String uuid, int major, int minor ) {

        Cursor rs = db.query(TABLA_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS[1] + " = '" + uuid +
                "' AND " + COLUMNAS_BEACON_ACTIONS[2] + " = " + major +
                " AND " + COLUMNAS_BEACON_ACTIONS[3] + " = " + minor, null, null, null, null);
        GwBeaconActions beaconActions = null;

        try {
            if (rs.moveToFirst()) {
                beaconActions = new GwBeaconActions(
                        rs.getInt(0),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getLong(4),
                        rs.getLong(5),
                        rs.getLong(6)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return beaconActions;
    }

    public GwBeaconActions selectBeaconActionsEnter( String uuid, int major, int minor ) {

        Cursor rs = db.query(TABLA_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS[1] + " = '" + uuid +
                "' AND " + COLUMNAS_BEACON_ACTIONS[2] + " = " + major +
                " AND " + COLUMNAS_BEACON_ACTIONS[3] + " = " + minor +
                " AND " + COLUMNAS_BEACON_ACTIONS[5] + " > 0 " +
                "AND " + COLUMNAS_BEACON_ACTIONS[4] + " = 0 ", null, null, null, null);
        GwBeaconActions beaconActions = null;

        try {
            if (rs.moveToFirst()) {
                beaconActions = new GwBeaconActions(
                        rs.getInt(0),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getLong(4),
                        rs.getLong(5),
                        rs.getLong(6)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return beaconActions;
    }

    public GwBeaconActions selectBeaconActionsRanging( String uuid, int major, int minor  ) {

        Cursor rs = db.query(TABLA_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS, COLUMNAS_BEACON_ACTIONS[1] + " = '" + uuid +
                "' AND " + COLUMNAS_BEACON_ACTIONS[2] + " = " + major +
                " AND " + COLUMNAS_BEACON_ACTIONS[3] + " = " + minor +
                " AND " + COLUMNAS_BEACON_ACTIONS[6] + " > 0 ", null, null, null, null);
        GwBeaconActions beaconActions = null;

        try {
            if (rs.moveToFirst()) {
                beaconActions = new GwBeaconActions(
                        rs.getInt(0),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getLong(4),
                        rs.getLong(5),
                        rs.getLong(6)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return beaconActions;
    }

    public ArrayList<GwMessages> selectNotificationEnter(String keyBeacon)
    {
        String[] columnas = { " N." + COLUMNAS_MESSAGES[0], " N." + COLUMNAS_MESSAGES[1], " N." + COLUMNAS_MESSAGES[2],
                " N." + COLUMNAS_MESSAGES[3], " N." + COLUMNAS_MESSAGES[4], " N." + COLUMNAS_MESSAGES[5],
                " N." + COLUMNAS_MESSAGES[6], " N." + COLUMNAS_MESSAGES[7], " N." + COLUMNAS_MESSAGES[8],
                " N." + COLUMNAS_MESSAGES[9]," N." + COLUMNAS_MESSAGES[10], " N." + COLUMNAS_MESSAGES[11],
                " N." + COLUMNAS_MESSAGES[12], " N." + COLUMNAS_MESSAGES[13], " N." + COLUMNAS_MESSAGES[14],
                " N." + COLUMNAS_MESSAGES[15], " N." + COLUMNAS_MESSAGES[16], " N." + COLUMNAS_MESSAGES[17]};

        Cursor rs = db.query(TABLA_MESSAGES + " N INNER JOIN " + TABLA_ASSOCIATION_NOTIFICATION + " B ON " + "N." + COLUMNAS_MESSAGES[0] + " = B." + COLUMNAS_ASSOCIATION_NOTIFICATION[1],
                columnas, " B." + COLUMNAS_ASSOCIATION_NOTIFICATION[0] + " = '" + keyBeacon + "' AND N." + COLUMNAS_MESSAGES[6] + " = -1 ", null, null, null, " B." + COLUMNAS_ASSOCIATION_NOTIFICATION[2]);

        ArrayList<GwMessages> gwMessages = new ArrayList<GwMessages>();
        if (rs.moveToFirst()) {
            do
            {
                gwMessages.add(getFromCursorGwMessages(rs));

            } while (rs.moveToNext());
        }
        rs.close();
        return gwMessages;
    }

    public GwMessages selectNotificationExit(String keyBeacon)
    {
        String[] columnas = { " N." + COLUMNAS_MESSAGES[0], " N." + COLUMNAS_MESSAGES[1], " N." + COLUMNAS_MESSAGES[2],
                " N." + COLUMNAS_MESSAGES[3], " N." + COLUMNAS_MESSAGES[4], " N." + COLUMNAS_MESSAGES[5],
                " N." + COLUMNAS_MESSAGES[6], " N." + COLUMNAS_MESSAGES[7], " N." + COLUMNAS_MESSAGES[8],
                " N." + COLUMNAS_MESSAGES[9]," N." + COLUMNAS_MESSAGES[10], " N." + COLUMNAS_MESSAGES[11],
                " N." + COLUMNAS_MESSAGES[12], " N." + COLUMNAS_MESSAGES[13], " N." + COLUMNAS_MESSAGES[14],
                " N." + COLUMNAS_MESSAGES[15], " N." + COLUMNAS_MESSAGES[16], " N." + COLUMNAS_MESSAGES[17]};

        Cursor rs = db.query(TABLA_MESSAGES + " N INNER JOIN " + TABLA_ASSOCIATION_NOTIFICATION + " B ON " + "N." + COLUMNAS_MESSAGES[0] + " = B." + COLUMNAS_ASSOCIATION_NOTIFICATION[1],
                columnas, " B." + COLUMNAS_ASSOCIATION_NOTIFICATION[0] + " = '" + keyBeacon + "' AND N." + COLUMNAS_MESSAGES[5] + " = -1 ", null, null, null, " B." + COLUMNAS_ASSOCIATION_NOTIFICATION[2]);

        GwMessages gwMessages = null;
        if (rs.moveToFirst()) {
            do
            {
                gwMessages = getFromCursorGwMessages(rs);

            } while (rs.moveToNext());
        }
        rs.close();
        return gwMessages;
    }

    public GwMessages selectMessage(String uid, int fila)
    {
        Cursor rs = db.query(TABLA_MESSAGES, COLUMNAS_MESSAGES, COLUMNAS_MESSAGES[1] + " = '" + uid + "' AND " + COLUMNAS_MESSAGES[0] + " = " + fila, null, null, null, null);
        GwMessages messages = null;

        try {
            if (rs.moveToFirst()) {
                messages = getFromCursorGwMessages(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return messages;
    }

    public GwMessages selectMessage(String uid)
    {
        Cursor rs = db.query(TABLA_MESSAGES, COLUMNAS_MESSAGES, COLUMNAS_MESSAGES[1] + " = '" + uid + "' ", null, null, null, null);
        GwMessages messages = null;

        try {
            if (rs.moveToFirst()) {
                messages = getFromCursorGwMessages(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return messages;
    }

    public GwMessages getFromCursorGwMessages(Cursor rs) {
        return new GwMessages(
                rs.getInt(0),
                rs.getString(1),
                rs.getInt(2) == 1 ? true : false,
                rs.getString(3),
                rs.getString(4),
                rs.getInt(5),
                rs.getInt(6),
                rs.getInt(7),
                rs.getInt(8),
                rs.getString(9),
                rs.getString(10),
                rs.getString(11),
                rs.getString(12),
                rs.getString(13),
                rs.getString(14),
                rs.getString(15),
                rs.getString(16),
                rs.getString(17)
        );
    }

    public GwNotifyTrigger selectLastNotifyTrigger(String uuid, int major, int minor)
    {
        Cursor rs = db.query(TABLA_NOTIFY_TRIGGER, COLUMNAS_NOTIFY_TRIGGER, COLUMNAS_NOTIFY_TRIGGER[0] + " = '" + uuid +
                "' AND " + COLUMNAS_NOTIFY_TRIGGER[2] + " = " + major +
                " AND " + COLUMNAS_NOTIFY_TRIGGER[3] + " = " + minor, null, null, null, " " + COLUMNAS_NOTIFY_TRIGGER[4] + " DESC");
        GwNotifyTrigger notifyTrigger = null;

        try {
            if (rs.moveToFirst()) {
                notifyTrigger = new GwNotifyTrigger(
                        rs.getString(0),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getLong(4)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return notifyTrigger;
    }

    public GwNotifyTrigger selectLastNotifyTrigger(String uuid, int major, int minor, String idNotification)
    {
        Cursor rs = db.query(TABLA_NOTIFY_TRIGGER, COLUMNAS_NOTIFY_TRIGGER, COLUMNAS_NOTIFY_TRIGGER[0] + " = '" + uuid +
                "' AND " + COLUMNAS_NOTIFY_TRIGGER[2] + " = " + major +
                " AND " + COLUMNAS_NOTIFY_TRIGGER[3] + " = " + minor +
                " AND " + COLUMNAS_NOTIFY_TRIGGER[1] + " ='" + idNotification + "'", null, null, null, " " + COLUMNAS_NOTIFY_TRIGGER[4] + " DESC");
        GwNotifyTrigger notifyTrigger = null;

        try {
            if (rs.moveToFirst()) {
                notifyTrigger = new GwNotifyTrigger(
                        rs.getString(0),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getLong(4)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return notifyTrigger;
    }

    public GwNotifyTrigger selectLastNotifyTriggerGeo(String uuidGeo)
    {
        Cursor rs = db.query(TABLA_NOTIFY_TRIGGER, COLUMNAS_NOTIFY_TRIGGER, COLUMNAS_NOTIFY_TRIGGER[0] + " = '" + uuidGeo +
                "' "
//                + COLUMNAS_NOTIFY_TRIGGER[1] + " ='" + uuidRule + "'"
                , null, null, null, " " + COLUMNAS_NOTIFY_TRIGGER[4] + " DESC");
        GwNotifyTrigger notifyTrigger = null;

        try {
            if (rs.moveToFirst()) {
                notifyTrigger = new GwNotifyTrigger(
                        rs.getString(0),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        rs.getLong(4)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return notifyTrigger;
    }

    public GwAssociationNotifications selectAssociationNotification(int keyNotification, String keyEvent)
    {
        Cursor rs = db.query(TABLA_ASSOCIATION_NOTIFICATION, COLUMNAS_ASSOCIATION_NOTIFICATION, COLUMNAS_ASSOCIATION_NOTIFICATION[1] + " = " + keyNotification + " AND " + COLUMNAS_ASSOCIATION_NOTIFICATION[0] + " = '" + keyEvent + "' ", null, null, null, null);
        GwAssociationNotifications beaconNotification = null;

        try {
            if (rs.moveToFirst()) {
                beaconNotification = new GwAssociationNotifications(
                        rs.getString(0),
                        rs.getInt(1),
                        rs.getInt(2)
                );
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return beaconNotification;
    }

    public GwUserActive selectUserActive()
    {
        Cursor rs = db.query(TABLA_USER_ACTIVE, COLUMNAS_USER_ACTIVE,null, null, null, null, null);
        GwUserActive userActive = null;

        try {
            if (rs.moveToFirst()) {
                userActive = new GwUserActive(
                        rs.getInt(0),
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return userActive;
    }

    public GwRegistry getFromCursorGwRegistry(Cursor rs) {
        return new GwRegistry(
                rs.getInt(0),
                rs.getString(1),
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getString(5),
                rs.getInt(6) == 1,
                rs.getDouble(7)
        );
    }

    public ArrayList<GwRegistry> selectRegistriesNotSyncro()
    {
        Cursor rs = db.query(TABLA_REGISTRY, COLUMNAS_REGISTRY, COLUMNAS_REGISTRY[6] + " = 0 ", null, null, null, COLUMNAS_REGISTRY[7] + " DESC LIMIT 150");

        ArrayList<GwRegistry> registries = new ArrayList<GwRegistry>();

        try {
            if (rs.moveToFirst()) {
                do {
                    registries.add(getFromCursorGwRegistry(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return registries;
    }

    public int numRegistriesNotSyncro()
    {
        Cursor rs = db.query(TABLA_REGISTRY, COLUMNAS_REGISTRY, COLUMNAS_REGISTRY[6] + " = 0 ", null, null, null, COLUMNAS_REGISTRY[7] + " DESC");

        ArrayList<GwRegistry> registries = new ArrayList<GwRegistry>();

        try {
            if (rs.moveToFirst()) {
                do {
                    registries.add(getFromCursorGwRegistry(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return registries.size();
    }

    public ArrayList<GwRegistry> selectRegistriesNotSyncroForDate( double syncro_date )
    {
        Cursor rs = db.query(TABLA_REGISTRY, COLUMNAS_REGISTRY, COLUMNAS_REGISTRY[6] + " = 0 AND " + COLUMNAS_REGISTRY[7] + " <= " + syncro_date + " ", null, null, null, COLUMNAS_REGISTRY[7] + " DESC LIMIT 150");

        ArrayList<GwRegistry> registries = new ArrayList<GwRegistry>();

        try {
            if (rs.moveToFirst()) {
                do {
                    registries.add(getFromCursorGwRegistry(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return registries;
    }

    public GwGeofence getFromCursorGwGeofence(Cursor rs)
    {
        return new GwGeofence(
                rs.getInt(0),
                rs.getString(1),
                rs.getString(2),
                rs.getFloat(3),
                rs.getFloat(4),
                rs.getInt(5)
              );
    }

    public ArrayList<GwGeofence> selectAllGeofences()
    {
        Cursor rs = db.query(TABLA_GEOFENCE, COLUMNAS_GEOFENCE, null, null, null, null, null);

        ArrayList<GwGeofence> gwGeofences = new ArrayList<GwGeofence>();

        try {
            if (rs.moveToFirst()) {
                do {
                    gwGeofences.add(getFromCursorGwGeofence(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return gwGeofences;
    }

    public GwRules getFromCursorGwRules(Cursor rs)
    {
        return new GwRules(
                rs.getInt(0),
                rs.getString(1),
                rs.getString(2),
                rs.getInt(3),
                rs.getInt(4),
                rs.getInt(5),
                rs.getInt(6),
                rs.getString(7),
                rs.getString(8),
                rs.getString(9),
                rs.getString(10)
        );
    }

    public GwRules selectRule( String uidChannel, String uidMessage )
    {
        Cursor rs = db.query(TABLA_RULES, COLUMNAS_RULES, COLUMNAS_RULES[1] + " ='" + uidChannel + "' AND " + COLUMNAS_RULES[7] + " ='" + uidMessage + "' ", null, null, null, null);
        GwRules rules = null;

        try {
            if (rs.moveToFirst()) {
                rules = getFromCursorGwRules(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return rules;
    }

    public ArrayList<GwRules> selectAllRulesToChannelEnterRegion( String uid )
    {
        Cursor rs = db.query(TABLA_RULES, COLUMNAS_RULES, COLUMNAS_RULES[1] + " ='" + uid + "' AND " + COLUMNAS_RULES[5] + " = -1 ", null, null, null, COLUMNAS_RULES[0] + " DESC");

        ArrayList<GwRules> gwRules = new ArrayList<>();

        try {
            if (rs.moveToFirst()) {
                do {
                    gwRules.add(getFromCursorGwRules(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return gwRules;
    }

    public ArrayList<GwRules> selectAllRulesToChannelExitRegion( String uid )
    {
        Cursor rs = db.query(TABLA_RULES, COLUMNAS_RULES, COLUMNAS_RULES[1] + " ='" + uid + "' AND " + COLUMNAS_RULES[4] + " = -1 ", null, null, null, COLUMNAS_RULES[0] + " DESC");

        ArrayList<GwRules> gwRules = new ArrayList<>();

        try {
            if (rs.moveToFirst()) {
                do {
                    gwRules.add(getFromCursorGwRules(rs));
                } while (rs.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }
        return gwRules;
    }

    public GwCampaign getFromCursorGwCampaign(Cursor rs)
    {
        return new GwCampaign(
                rs.getInt(0),
                rs.getString(1),
                rs.getString(2),
                rs.getInt(3),
                rs.getInt(4),
                rs.getInt(5),
                rs.getInt(6),
                rs.getInt(7)
        );
    }

    public GwCampaign selectCampaign( String uid ) {

        Cursor rs = db.query(TABLA_CAMPAIGN, COLUMNAS_CAMPAIGN, COLUMNAS_CAMPAIGN[1] + " = '" + uid + "' ", null, null, null, null);
        GwCampaign campaign = null;

        try {
            if (rs.moveToFirst()) {
                campaign = getFromCursorGwCampaign(rs);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rs.close();
        }

        return campaign;
    }

    //************************************************************************
    //FUNCIÓN QUE DEVUELVE EL CURSOR QUE APUNTA A LOS RESULTADOS TRAS LA CONSULTA QUERY REALIZADA
    public Cursor query(String nombreTabla, String[] campos, String selection, String[] selectionArgs, String groupby, String having, String orderby) {
        Cursor cursor = db.query(nombreTabla, campos, selection, selectionArgs, groupby, having, orderby);
        return cursor;
    }

    //************************************************************************
    // UPDATE

    public long actualizarBeaconActions(long fila, GwBeaconActions beaconActions)
    {

        // Creamos el objeto utilizado para definir el contenido a actualizar
        ContentValues valoresActualizar = new ContentValues();

        // Asignamos valores a las columnas correspondientes
        valoresActualizar.put(COLUMNAS_BEACON_ACTIONS[1], beaconActions.getUUID());
        valoresActualizar.put(COLUMNAS_BEACON_ACTIONS[2], beaconActions.getMajor());
        valoresActualizar.put(COLUMNAS_BEACON_ACTIONS[3], beaconActions.getMinor());
        valoresActualizar.put(COLUMNAS_BEACON_ACTIONS[4], beaconActions.getLast_exit());
        valoresActualizar.put(COLUMNAS_BEACON_ACTIONS[5], beaconActions.getLast_enter());
        valoresActualizar.put(COLUMNAS_BEACON_ACTIONS[6], beaconActions.getLast_range_notify());

        String where = COLUMNAS_BEACON_ACTIONS[0] + "=" + fila;

        // Actualizamos la fila con el índice especificado en la instrucción anterior
        return db.update(TABLA_BEACON_ACTIONS, valoresActualizar, where, null);

    }

    public long actualizarMessages(long fila, GwMessages messages)
    {

        // Creamos el objeto utilizado para definir el contenido a actualizar
        ContentValues valoresActualizar = new ContentValues();

        // Asignamos valores a las columnas correspondientes
        valoresActualizar.put(COLUMNAS_MESSAGES[1], messages.getUid());
        valoresActualizar.put(COLUMNAS_MESSAGES[2], messages.isShow());
        valoresActualizar.put(COLUMNAS_MESSAGES[3], messages.getTitle());
        valoresActualizar.put(COLUMNAS_MESSAGES[4], messages.getContent());
        valoresActualizar.put(COLUMNAS_MESSAGES[5], messages.getEnterInterval());
        valoresActualizar.put(COLUMNAS_MESSAGES[6], messages.getExitInterval());
        valoresActualizar.put(COLUMNAS_MESSAGES[7], messages.getDistance());
        valoresActualizar.put(COLUMNAS_MESSAGES[8], messages.getRelaunchInterval());
        valoresActualizar.put(COLUMNAS_MESSAGES[9], messages.getViewId());
        valoresActualizar.put(COLUMNAS_MESSAGES[10], messages.getUrl());
        valoresActualizar.put(COLUMNAS_MESSAGES[11], messages.getImagePath());
        valoresActualizar.put(COLUMNAS_MESSAGES[12], messages.getCampaign());
        valoresActualizar.put(COLUMNAS_MESSAGES[13], messages.getWeekdays());
        valoresActualizar.put(COLUMNAS_MESSAGES[14], messages.getStartTime());
        valoresActualizar.put(COLUMNAS_MESSAGES[15], messages.getEndTime());
        valoresActualizar.put(COLUMNAS_MESSAGES[16], messages.getImageNotifPath());
        valoresActualizar.put(COLUMNAS_MESSAGES[17], messages.getSound());

        String where = COLUMNAS_MESSAGES[0] + "=" + fila;

        // Actualizamos la fila con el índice especificado en la instrucción anterior
        return db.update(TABLA_MESSAGES, valoresActualizar, where, null);

    }

    public long actualizarRule(long fila, GwRules rules)
    {

        // Creamos el objeto utilizado para definir el contenido a actualizar
        ContentValues valoresActualizar = new ContentValues();

        // Asignamos valores a las columnas correspondientes
        valoresActualizar.put(COLUMNAS_RULES[1], rules.getUidChanel());
        valoresActualizar.put(COLUMNAS_RULES[2], rules.getCampaign());
        valoresActualizar.put(COLUMNAS_RULES[3], rules.getDistance());
        valoresActualizar.put(COLUMNAS_RULES[4], rules.getEnterInterval());
        valoresActualizar.put(COLUMNAS_RULES[5], rules.getExitInterval());
        valoresActualizar.put(COLUMNAS_RULES[6], rules.getRelaunchInterval());
        valoresActualizar.put(COLUMNAS_RULES[7], rules.getUidMessage());
        valoresActualizar.put(COLUMNAS_RULES[8], rules.getWeekdays());
        valoresActualizar.put(COLUMNAS_RULES[9], rules.getStartTime());
        valoresActualizar.put(COLUMNAS_RULES[10], rules.getEndTime());

        String where = COLUMNAS_RULES[0] + "=" + fila;

        // Actualizamos la fila con el índice especificado en la instrucción anterior
        return db.update(TABLA_RULES, valoresActualizar, where, null);

    }

    public long actualizarRegistrySyncronized(long fila, Double syncroDate )
    {

        // Creamos el objeto utilizado para definir el contenido a actualizar
        ContentValues valoresActualizar = new ContentValues();

        // Asignamos valores a las columnas correspondientes
        valoresActualizar.put(COLUMNAS_REGISTRY[6], 1);

        String where = COLUMNAS_REGISTRY[0] + "=" + fila;

        // Actualizamos la fila con el índice especificado en la instrucción anterior
        return db.update(TABLA_REGISTRY, valoresActualizar, where, null);

    }

    //************************************************************************
    //    UPSERT

    public long upsertBeaconActions(GwBeaconActions beaconActions) {

        //Si existe, actualiza, sino inserta
        GwBeaconActions gwBeaconActions = null;
        if( beaconActions.getLast_range_notify() > 0 )
        {
            gwBeaconActions = selectBeaconActionsRanging(beaconActions.getUUID(), beaconActions.getMajor(), beaconActions.getMinor());
        }
        else
        {
            gwBeaconActions = selectBeaconActionsEnter(beaconActions.getUUID(), beaconActions.getMajor(), beaconActions.getMinor());
        }
        if (gwBeaconActions == null) {
            return insertBeacon_action(beaconActions);
        } else {
            beaconActions.setId(gwBeaconActions.getId());
            return actualizarBeaconActions(beaconActions.getId(), beaconActions);
        }
    }

    public long upsertMessages(GwMessages messages) {

        //Si existe, actualiza, sino inserta
        GwMessages messages1 = null;

        messages1 = selectMessage(messages.getUid(), messages.getId());

        if (messages1 == null) {
            return insertMessages(messages);
        } else {
            messages.setId(messages1.getId());
            return actualizarMessages(messages.getId(), messages);
        }
    }

    public long upsertRules(GwRules rules) {

        //Si existe, actualiza, sino inserta
        GwRules rules1 = null;

        rules1 = selectRule(rules.getUidChanel(), rules.getUidMessage());

        if (rules1 == null) {
            return insertRule(rules);
        } else {
            rules.set_id(rules1.get_id());
            return actualizarRule(rules.get_id(), rules);
        }
    }


    //************************************************************************
    // SQLITEOPENHELPER
    private static class MiOpenHelper extends SQLiteOpenHelper {


        private Context contexto;

        public MiOpenHelper(Context contexto, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
            super(contexto, nombre, factory, version);
            this.contexto = contexto;

            SQLiteDatabase checkDB = null;

            try {
                String myPath = GwConfig.getDbPath(contexto);
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            } catch (SQLiteException e) {
                Log.i( TAG, "Error al abrir base de datos en openHelper => NO existe, se creará.");
                if (checkDB != null) checkDB.close();
            }

            if (checkDB != null) {
                try {
                    Log.i(TAG, "Versión BD: " + checkDB.getVersion());
                    onUpgrade(checkDB, checkDB.getVersion(), VERSION_BASE_DATOS);
                }
                catch (SQLiteDiskIOException ex) {
                    ex.printStackTrace();
                }
            }
            else {
                createDataBase();
            }
        }


        public SQLiteDatabase openDataBase() throws SQLException {

            //Open the database
            String myPath = GwConfig.getDbPath(contexto);
            return SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
                Log.i(TAG, "onCreate BD pre");
                db.execSQL(CREATE_DB);
                Log.i(TAG, "onCreate BD post");

        }

        public void createDataBase() {

            boolean dbExist = checkDataBase();

            if (dbExist) {
                Log.i(TAG, "BD creada" );
            }
            else {
                //By calling this method and empty database will be created into the default system path
                //of your application so we are gonna be able to overwrite that database with our database.
                this.getReadableDatabase();//Llama al método onCreate donde copia la BD
            }
        }

        private boolean checkDataBase() {

            SQLiteDatabase checkDB = null;

            try {
                String myPath = GwConfig.getDbPath(contexto);
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            }
            catch (SQLiteException e) {
                Log.i("Gwella", "Error al abrir base de datos en checkBD => NO existe");
                if (checkDB != null) checkDB.close();
            }

            if (checkDB != null) {
                checkDB.close();
            }

            return checkDB != null;
        }


        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.i("Gwella", "onUpgrade BD");

            if( oldVersion < newVersion ) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_BEACON_ACTIONS);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_BEACON);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_ASSOCIATION_NOTIFICATION);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_MESSAGES);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_NOTIFY_TRIGGER);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_REGISTRY);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_USER_ACTIVE);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_GEOFENCE);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_RULES);
                db.execSQL("DROP TABLE IF EXISTS " + TABLA_CAMPAIGN);
                onCreate(db);
            }
        }
    }
}
