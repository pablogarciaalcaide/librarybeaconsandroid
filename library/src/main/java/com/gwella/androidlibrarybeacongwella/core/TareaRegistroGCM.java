package com.gwella.androidlibrarybeacongwella.core;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * @author juangra on 10/12/15.
 */
public class TareaRegistroGCM extends AsyncTask<String, Integer, Void>
{
    private GoogleCloudMessaging gcm;
    private Context context;

    public TareaRegistroGCM( Context context )
    {
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... params)
    {
        try
        {
            if (gcm == null)
            {
                gcm = GoogleCloudMessaging.getInstance(context);
            }

            //Nos registramos en los servidores de GCM
            String regid = gcm.register(GwConstants.SENDER_ID);

            Log.d("TareaRegistroGCM", "Registrado en GCM: registration_id=" + regid);

            if ( regid.length() > 0 )
            {
//                //Si viene update habrá que guardar el antiguo registro y poner flag para que lo envie.
//                if ( params[0].equals(GwConstants.K_UPDATED) )
//                {
//                    //Debemos enviar el antiguo registration_id y el nuevo para actualizarlo
//                    GwConstants.setSharedPreferences(context, GwConstants.PROPERTY_TOKEN_GOOGLE_OLD, GwConstants.getRegistrationIdGoogle(context));
//                    GwConstants.sendActualizeTokenGoogle = true;
//                }

                //Guardamos los datos del registro
                GwConstants.setRegistrationId(context, regid);

                GwConstants.inicializarProcesoPrincipal(context);
            }

        }
        catch (IOException ex)
        {
            Log.d("TareaRegistroGCM", "Error registro en GCM:" + ex.getMessage());
        }

        return null;
    }
}
