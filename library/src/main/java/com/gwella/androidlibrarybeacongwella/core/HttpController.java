package com.gwella.androidlibrarybeacongwella.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.gwella.androidlibrarybeacongwella.BuildConfig;
import com.gwella.androidlibrarybeacongwella.model.GwAdvertising;
import com.gwella.androidlibrarybeacongwella.model.GwAssociationNotifications;
import com.gwella.androidlibrarybeacongwella.model.GwBeacon;
import com.gwella.androidlibrarybeacongwella.model.GwCampaign;
import com.gwella.androidlibrarybeacongwella.model.GwGeofence;
import com.gwella.androidlibrarybeacongwella.model.GwMessages;
import com.gwella.androidlibrarybeacongwella.model.GwMetadata;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;
import com.gwella.androidlibrarybeacongwella.model.GwRules;
import com.gwella.androidlibrarybeacongwella.model.GwUserActive;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author juangra on 24/11/15.
 */
public class HttpController
{
    private static HttpController bd_singleton;

    private Context context;
    private static String TAG = "HttpController";

    private final String INSTALLATION = "installation";
    private final String LOCATION = "location";
    private final String REGISTRY = "registry";
    private final String APPLICATION = "application";
    private final String METADATA = "metadata";
    private final String ALERTS = "alerts";
    private final String ADVERTISING = "advertising";

    private String coordinatesAux = "";
    private List<GwMetadata> gwMetadataListAux = new ArrayList<>();
    private List<GwAdvertising> gwAdvertisingList = new ArrayList<>();
    private VolleyCallback volleyCallback;

    private RequestQueue mRequestQueue;
    private ImageLoader imageLoader;

    // Instantiate the cache
    private int cache = 1024 * 1024 * 2; // 2MB cap


    // CONSTRUCTOR
    public synchronized static HttpController getInstance(Context context) {
        if (bd_singleton == null) {
            bd_singleton = new HttpController(context.getApplicationContext());
        }
        return bd_singleton;
    }

    public HttpController( Context context )
    {
        this.context = context;
        mRequestQueue = getRequestQueue();

        imageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<>(40);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });

    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }


    public void sendAuthenticationToken( final String metodoEntrada )
    {
        String urlInstallation = GwEnviroment.URL_DEFECT + "access";

        Map<String, String> postParam= new HashMap<String, String>();
        postParam.put("grant_type", "client_credentials");
        postParam.put("client_id", GwConstants.getStringRes("gwella_client_id"));
        postParam.put("client_secret", GwConstants.getStringRes("gwella_client_token"));


        //urlInstallation, new JSONObject(postParam),
        //                new Response.Listener<JSONObject>()
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlInstallation, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());
                            GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);
                            bd.deleteAllUserActive();
                            try
                            {
                                if( response.has("token") ) GwConstants.setSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID, response.getString("token"));
                                if( response.has("expires_in") ) GwConstants.setSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_EXPIRE, response.getString("expires_in"));
                                String dateMls = String.valueOf(System.currentTimeMillis());
                                GwConstants.setSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_DATE, dateMls);
                            }
                            catch ( JSONException e )
                            {
                                e.printStackTrace();
                            }
                            switch ( metodoEntrada )
                            {
                                case INSTALLATION:
                                    sendInstallation();
                                    break;
                                case LOCATION:
                                    sendLocation(coordinatesAux);
                                    break;
                                case REGISTRY:
                                    crearJsonRegistrosAndSend();
                                    break;
                                case APPLICATION:
                                    sendInfo();
                                    break;
                                case METADATA:
                                    sendMetadata(gwMetadataListAux);
                                    break;
                                case ALERTS:
                                    getAlerts(volleyCallback);
                                    break;
                                case ADVERTISING:
                                    sendMetadataAdvertising(gwAdvertisingList);
                                    break;
                            }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TAG = "SendAuthToken";
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };

        // Adding request to request queue
        String body = new String( jsonObjReq.getBody() );
        Log.d(TAG, "Access body: " + body );

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(jsonObjReq);
    }

    public void sendInstallation()
    {
        if( checkAuthorization() )
        {

            String urlInstallation = GwEnviroment.URL_DEFECT + "v1/installation";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);

            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("app_name", context.getPackageName());
            postParam.put("app_version", GwConstants.getVersion(context));
            postParam.put("device_type", "Android");
            postParam.put("device_id", GwConstants.getDeviceId(context));
            postParam.put("device_token", deviceToken );
            postParam.put("environment", GwEnviroment.PROPERTY_ENVIROMENT);
            postParam.put("sdk_version", BuildConfig.VERSION_NAME);
            postParam.put("language", Locale.getDefault().getLanguage());

//            if ( GwConstants.sendActualizeTokenGoogle )
//            {
//                postParam.put("old_device_token", GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_TOKEN_GOOGLE_OLD));
//                GwConstants.sendActualizeTokenGoogle = false;
//            }

            RequestCustom jsonObjReq = new RequestCustom(Request.Method.POST, urlInstallation, new JSONObject(postParam), new Response.Listener<JSONObject>()
            {

                @Override
                public void onResponse(JSONObject response)
                {
                    Log.d(TAG, response.toString());
                    try
                    {
                        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);
                        bd.deleteAllUserActive();

                        GwUserActive userActive = new GwUserActive(0, GwConstants.getStringRes("gwella_client_token"), GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_TOKEN_GOOGLE), response.getString("app_name"), response.getString("app_version"), response.getString("device_type"));

                        bd.insertUserActive(userActive);

                    }catch ( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    new GwCargarBD(context).cargarBDVolley();

                }
            }, new Response.ErrorListener()
            {

                @Override
                public void onErrorResponse(VolleyError error)
                {
                    TAG = "SendInstall";

                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    processErrorServer(error, INSTALLATION);
                }
            })
            {

                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    return headers;
                }

            };

            // Adding request to request queue
            String body = new String(jsonObjReq.getBody());
            Log.d(TAG, "Installation body: " + body);
            addToRequestQueue(jsonObjReq);
        }
        else
        {
            sendAuthenticationToken(INSTALLATION);
        }
    }

    private boolean checkAuthorization()
    {
        String authorizationId = GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID);
        if( authorizationId.length() > 0 )
        {
            //Comprobar si a caducado el authorization id
            long dateActually = System.currentTimeMillis();
            long dateAuthorization = Long.parseLong(GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_DATE));
            int expirationTime = Integer.parseInt(GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_EXPIRE));

            long timeLapsed = (dateActually - dateAuthorization) / 1000 ; //Convierto milisegundos a segundos

            return timeLapsed <= expirationTime;
        }
        else
        {
            return false;
        }
    }

    public void sendLocation( String coordinates )
    {
        coordinatesAux = coordinates;
        if( checkAuthorization() )
        {

            String urlInstallation = GwEnviroment.URL_DEFECT + "v1/location";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);
            String advertisingId;
            if( GwConstants.hasSharedPreferences(context, GwConstants.ADVERTISING_ID) )
            {
                advertisingId = GwConstants.getSharedPreferences(context, GwConstants.ADVERTISING_ID);
            }
            else
            {
                advertisingId = GwConstants.getAdvertaisingId();
                if( advertisingId != null ){
                    GwConstants.setSharedPreferences(context, GwConstants.ADVERTISING_ID, advertisingId);
                }
            }

            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("coordinates", coordinates);
//            if( advertisingId != null )
//            {
                postParam.put("advertising_id", advertisingId);
//            }

            //Create list GwAdvertising y enviarlos
            List<GwAdvertising> advertisingList = new ArrayList<>();

            advertisingList.add(new GwAdvertising("ANDROID_ID", GwAdvertising.TYPE_STRING, GwConstants.getSecureAndroidId(), context ));
            advertisingList.add(new GwAdvertising("serial", GwAdvertising.TYPE_STRING, Build.SERIAL, context ));
            advertisingList.add(new GwAdvertising("model", GwAdvertising.TYPE_STRING, Build.DEVICE, context ));
            advertisingList.add(new GwAdvertising("id", GwAdvertising.TYPE_STRING, Build.ID, context ));
            advertisingList.add(new GwAdvertising("sdk", GwAdvertising.TYPE_STRING, String.valueOf(Build.VERSION.SDK_INT), context ));
            advertisingList.add(new GwAdvertising("version_code", GwAdvertising.TYPE_STRING, Build.VERSION.RELEASE, context ));

            sendMetadataAdvertising(advertisingList);

            RequestCustom jsonObjReq = new RequestCustom(Request.Method.POST, urlInstallation, new JSONObject(postParam), new Response.Listener<JSONObject>()
            {

                @Override
                public void onResponse(JSONObject response)
                {
                    Log.d(TAG, response.toString());

                }
            }, new Response.ErrorListener()
            {

                @Override
                public void onErrorResponse(VolleyError error)
                {
                    TAG = "SendAuthToken";

                    VolleyLog.d(TAG, "Error: " + error.getMessage());

                    processErrorServer(error, LOCATION);

                }
            })
            {

                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    headers.put("X-Device-Token", deviceToken);
                    return headers;
                }

            };

            // Adding request to request queue
            String body = new String(jsonObjReq.getBody());
            Log.d(TAG, "Location body: " + body);
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            addToRequestQueue(jsonObjReq);
        }
        else
        {
            sendAuthenticationToken(LOCATION);
        }
    }

    public void sendRegistry( JSONObject jsonObject )
    {
        if( checkAuthorization() )
        {
            String urlInstallation = GwEnviroment.URL_DEFECT + "v1/registry";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);

            RequestCustom jsonObjReq = new RequestCustom(Request.Method.POST, urlInstallation, jsonObject, new Response.Listener<JSONObject>()
            {

                @Override
                public void onResponse(JSONObject response)
                {
                    Log.d(TAG, response.toString());
                    GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);
                    try
                    {
                        double dateSyncro = response.getDouble("syncro");

                        ArrayList<GwRegistry> registries = bd.selectRegistriesNotSyncroForDate(dateSyncro);
                        Log.i(TAG, "Registros actualizados: " + registries.size());
                        for ( int i = 0; i < registries.size(); i++ )
                        {
                            bd.actualizarRegistrySyncronized(registries.get(i).get_id(), dateSyncro);
                        }
                    }catch ( JSONException e )
                    {
                        e.printStackTrace();
                    }

                    int numRegistros = bd.numRegistriesNotSyncro();
                    if( numRegistros > 0 )
                    {
                        Log.i(TAG, "Registros sin sincronizar: " + numRegistros);
                        crearJsonRegistrosAndSend();
                    }
                    else{
                        bd.deleteRegistriesSincronized();
                    }
                }
            }, new Response.ErrorListener()
            {

                @Override
                public void onErrorResponse(VolleyError error)
                {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    processErrorServer(error, REGISTRY);
                }
            })
            {

                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    headers.put("X-Device-Token", deviceToken);
                    return headers;
                }

            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            // Adding request to request queue
            String body = new String(jsonObjReq.getBody());
            Log.d(TAG, "Registry body: " + body);
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            addToRequestQueue(jsonObjReq);
        }
        else
        {
            sendAuthenticationToken(REGISTRY);
        }
    }

    public void crearJsonRegistrosAndSend()
    {
        ArrayList<GwRegistry> registries = new ArrayList<>();
        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);

        registries = bd.selectRegistriesNotSyncro();

        if( registries.size() > 0 )
        {
            double dateSyncro = System.currentTimeMillis();
            JSONObject json_principal = new JSONObject();
            try
            {
                JSONArray jsonArray = new JSONArray();
                Log.i(TAG, "Registros enviados: " + registries.size() );
                for( int i = 0; i < registries.size(); i++ )
                {
                    JSONObject j_registry = new JSONObject();

                    if( registries.get(i).getData().length() > 0 )
                    {
                        j_registry.put("data", new JSONObject(registries.get(i).getData()));
                    }

                    j_registry.put("source", registries.get(i).getSource());
                    j_registry.put("uid", registries.get(i).getUid());
                    j_registry.put("event", registries.get(i).getAction());
                    j_registry.put("date", registries.get(i).getDateCreated());

                    jsonArray.put(j_registry);
                }

                json_principal.put( "registries", jsonArray );
                json_principal.put("syncro", dateSyncro);

            }catch ( JSONException e )
            {
                e.printStackTrace();
            }

            HttpController.getInstance(context).sendRegistry(json_principal);
        }
    }

    public void sendInfo()
    {
        if( checkAuthorization() )
        {
            String urlInstallation = GwEnviroment.URL_DEFECT + "v1/info";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlInstallation,null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, response.toString());

                    GwMiAdapterBD bd = GwMiAdapterBD.getInstance(context);

                    bd.deleteAllBeacon();
                    bd.deleteAllAssociationNotification();
                    bd.deleteAllMessages();
                    bd.deleteAllGeofence();
                    bd.deleteAllRules();
                    bd.deleteAllCampaign();

                    try {
//                        JSONObject jsonMain = response.getJSONObject("application");
                        JSONObject jCompany = response.getJSONObject("company");

                        //Guardo la url del icono de la app para usarlo en la notificación
                        if (jCompany.has("icon_url")) {
                            String urlIcon = jCompany.getString("icon_url");
                            if (urlIcon.length() > 2) {
                                GwConstants.setSharedPreferences(context, GwConstants.URL_ICON_APP, urlIcon);
                            }
                        }

                        //Guardo el name de la company y el uuid
                        GwConstants.setSharedPreferences(context, GwConstants.IDENTIFIER_COMPANY, jCompany.getString("name"));
                        String uuid = jCompany.getString("uuid");
                        GwConstants.setSharedPreferences(context, GwConstants.UUID_COMPANY, uuid);

                        //Guardo las campaigns
                        JSONArray jCampaign = response.getJSONArray("campaigns");
                        JSONObject campaign;
                        for (int i = 0; i < jCampaign.length(); i++) {
                            campaign = jCampaign.getJSONObject(i);
                            String uidCampaign = campaign.getString("uid");
                            String nameCampaign = campaign.getString("name");
                            String starDate = campaign.getString("start_date");
                            String endDate = campaign.getString("end_date");
                            long starDateMl = GwConstants.formatingDateToServer(starDate).getTime();
                            long endDateMl = GwConstants.formatingDateToServer(endDate).getTime();

                            bd.insertCampaign(new GwCampaign(
                                    0,
                                    uidCampaign,
                                    nameCampaign,
                                    starDateMl,
                                    endDateMl,
                                    campaign.getInt("messages_limit"),
                                    campaign.getInt("messages_count"),
                                    campaign.getInt("user_messages_limit")
                            ));
                        }
                        //End Campaigns

                        //Start messages
                        JSONArray j_messages = response.getJSONArray("messages");
                        JSONObject message;
                        for (int i = 0; i < j_messages.length(); i++) {
                            message = j_messages.getJSONObject(i);

                            bd.insertMessages(new GwMessages(
                                    0,
                                    message.getString("uid"),
                                    message.getBoolean("show"),
                                    message.getString("title"),
                                    message.getString("content"),
                                    0,
                                    0,
                                    0,
                                    0,
                                    message.getString("view_id"),
                                    message.getString("url"),
                                    message.getString("image_path"),
                                    message.getString("campaign"),
                                    "",
                                    "",
                                    "",
                                    message.getString("imageNotifPath"),
                                    message.getString("sound"))
                            );
                        }
                        //End messages

                        //Start Beacons
                        JSONArray j_beacons = response.getJSONArray("beacons");
                        if (j_beacons.length() <= 0) {
                            GwConstants.BEACONS_ACTIVE = false;
                        }
                        JSONObject beacon;
                        for (int i = 0; i < j_beacons.length(); i++) {
                            beacon = j_beacons.getJSONObject(i);

                            bd.insertBeacon(new GwBeacon(uuid, beacon.getInt("major"), beacon.getInt("minor"), beacon.getString("uid"), beacon.optBoolean("isMonitoring")));
                            JSONArray j_rules = beacon.getJSONArray("rules");
                            JSONObject rule;
                            for (int k = 0; k < j_rules.length(); k++) {
                                rule = j_rules.getJSONObject(k);

                                GwMessages mesAux = bd.selectMessage(rule.getString("message"));

                                long idMessage = bd.insertMessages(new GwMessages(
                                        0,
                                        rule.getString("message"),
                                        mesAux.isShow(),
                                        mesAux.getTitle(),
                                        mesAux.getContent(),
                                        rule.optInt("enter_interval", -1),
                                        rule.optInt("exit_interval", -1),
                                        rule.optInt("distance", 0),
                                        rule.optInt("relaunch_interval", -1),
                                        mesAux.getViewId(),
                                        mesAux.getUrl(),
                                        mesAux.getImagePath(),
                                        rule.getString("campaign"),
                                        rule.getJSONArray("weekdays").toString().replace("\"", ""),
                                        rule.getString("start_time"),
                                        rule.getString("end_time"),
                                        mesAux.getImageNotifPath(),
                                        mesAux.getSound())
                                );

                                bd.insertAssociationNotification(new GwAssociationNotifications(
                                        beacon.getString("uid"),
                                        (int) idMessage,
                                        k
                                ));
                            }

                        }
                        //End beacons

                        //Todo: añadir los beacons monitoring

                        //Recupero los geofences
                        JSONArray jGeofences = response.getJSONArray("geos");
                        GwGeofence geofence;
                        if (jGeofences.length() <= 0) {
                            GwConstants.GEOFENCING_ACTIVE = false;
                        }
                        for (int i = 0; i < jGeofences.length(); i++) {
                            JSONObject jGeo = jGeofences.getJSONObject(i);

                            JSONObject jGeometryMain = jGeo.getJSONObject("geometry");

                            JSONArray jFeatures = jGeometryMain.getJSONArray("features");
                            JSONObject jFeature = jFeatures.getJSONObject(0);

                            JSONObject jGeometry = jFeature.getJSONObject("geometry");
                            JSONObject jProperties = jFeature.getJSONObject("properties");

                            JSONArray jCoordinates = jGeometry.getJSONArray("coordinates");
                            float latitude = (float) jCoordinates.getDouble(0);
                            float longitude = (float) jCoordinates.getDouble(1);


                            JSONObject geoJSON = new JSONObject(jGeo.getString("geometry"));
                            geofence = new GwGeofence(0, jGeo.getString("uid"), jGeometry.getString("type"), latitude, longitude, Integer.valueOf(jProperties.getString("radius")));

                            bd.insertGeofence(geofence);

                            //Recupero las rules del geo
                            JSONArray j_Rules = jGeo.getJSONArray("rules");
                            GwRules rule;
                            for (int j = 0; j < j_Rules.length(); j++) {
                                JSONObject jRule = j_Rules.getJSONObject(j);
                                rule = new GwRules(
                                        0,
                                        jGeo.getString("uid"),
                                        jRule.getString("campaign"),
                                        (int) jRule.optDouble("distance", 0),
                                        (int) jRule.optDouble("enter_interval", -1),
                                        (int) jRule.optDouble("exit_interval", -1),
                                        (int) jRule.optDouble("relaunch_interval", -1),
                                        jRule.getString("message"),
                                        jRule.getJSONArray("weekdays").toString().replace("\"", ""),
                                        jRule.getString("start_time"),
                                        jRule.getString("end_time")
                                );

                                bd.upsertRules(rule);
                            }
                        }

                        //End Geofences


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ProcesoPrincipal.getInstance(context).inicializarBeacons();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    processErrorServer(error, APPLICATION);
                }
            } ) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    headers.put("X-Device-Token", deviceToken);
                    return headers;
                }
            };

            // Adding request to request queue
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            HttpController.getInstance(context).addToRequestQueue(jsonObjReq);
        }
        else
        {
            sendAuthenticationToken(APPLICATION);
        }
    }

    public void sendMetadata(List<GwMetadata> metadataList)
    {
        if( checkAuthorization() )
        {
            String urlMetadata = GwEnviroment.URL_DEFECT + "v1/installation/metadata";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);

            //Crear json Metadatos

            JSONObject jsonMain = new JSONObject();
            try
            {
                JSONArray jsonMetadata = new JSONArray();
                for ( GwMetadata gwMetadata : metadataList )
                {
                    JSONObject jsonObject = new JSONObject(gwMetadata.toString());
                    jsonMetadata.put(jsonObject);
                }

                jsonMain.put(METADATA, jsonMetadata);
            }catch ( JSONException e )
            {
                e.printStackTrace();
            }
            /**
             * Este código es para que en desarrollo se salte la verificación del certificado autofirmado del
             * servidor de desarrollo.
             */
            if( GwEnviroment.PROPERTY_ENVIROMENT.equals(GwConstants.ENVIROMENT_DEVELOPMENT) )
            {
                HttpsTrustManager.allowAllSSL();
            }

            RequestJsonCustom jsonObjReq = new RequestJsonCustom(Request.Method.POST, urlMetadata, jsonMain, new Response.Listener<JSONObject>()
            {

                @Override
                public void onResponse(JSONObject response)
                {
                    Log.d(TAG, response.toString());
                }
            }, new Response.ErrorListener()
            {

                @Override
                public void onErrorResponse(VolleyError error)
                {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    processErrorServer(error, METADATA);
                }
            })
            {

                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    headers.put("X-Device-Token", deviceToken);
                    return headers;
                }

            };

            // Adding request to request queue
            String body = new String(jsonObjReq.getBody());
            Log.d(TAG, "Meatada body: " + body);
            addToRequestQueue(jsonObjReq);
        }
        else
        {
            gwMetadataListAux = metadataList;
            sendAuthenticationToken(METADATA);
        }
    }


    public void getAlerts(final VolleyCallback callback) {
        volleyCallback = callback;
        if( checkAuthorization() )
        {

            String urlInstallation = GwEnviroment.URL_DEFECT + "v1/received";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);

            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("device_token", deviceToken);


            //TODO enviar parametros??
            JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, urlInstallation, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.d(TAG, response.toString());
                    callback.onSuccess(response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());

                    processErrorServer(error, LOCATION);
                    callback.failed(error);
                }
            })
            {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    headers.put("X-Device-Token", deviceToken);
                    return headers;
                }

            };

            // Adding request to request queue
            String body = new String(jsonObjReq.getBody());
            Log.d(TAG, "Alerts body: " + body);
            addToRequestQueue(jsonObjReq);
        }
        else
        {
            sendAuthenticationToken(ALERTS);
        }
    }

    public void sendMetadataAdvertising(List<GwAdvertising> advertisingList)
    {
        if( checkAuthorization() )
        {
            String urlAdvertising = GwEnviroment.URL_DEFECT + "v1/advertising";
            final String deviceToken = GwConstants.getRegistrationIdGoogle(context);

            JSONObject jsonMain = new JSONObject();
            try
            {
                double dateSyncro = System.currentTimeMillis();
                JSONArray jsonArray = new JSONArray();
                for ( GwAdvertising gwAdvertising : advertisingList )
                {
                    JSONObject jsonObject = new JSONObject(gwAdvertising.toString());
                    jsonArray.put(jsonObject);
                }

                jsonMain.put("registries", jsonArray);
                jsonMain.put("syncro", dateSyncro);
            }catch ( JSONException e )
            {
                e.printStackTrace();
            }
            /**
             * Este código es para que en desarrollo se salte la verificación del certificado autofirmado del
             * servidor de desarrollo.
             */
            if( GwEnviroment.PROPERTY_ENVIROMENT.equals(GwConstants.ENVIROMENT_DEVELOPMENT) )
            {
                HttpsTrustManager.allowAllSSL();
            }

            RequestJsonCustom jsonObjReq = new RequestJsonCustom(Request.Method.POST, urlAdvertising, jsonMain, new Response.Listener<JSONObject>()
            {

                @Override
                public void onResponse(JSONObject response)
                {
                    Log.d(TAG, response.toString());
                }
            }, new Response.ErrorListener()
            {

                @Override
                public void onErrorResponse(VolleyError error)
                {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    processErrorServer(error, METADATA);
                }
            })
            {

                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError
                {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + GwConstants.getSharedPreferences(context, GwConstants.PROPERTY_AUTHENTICATION_ID));
                    headers.put("X-Device-Token", deviceToken);
                    return headers;
                }

            };

            // Adding request to request queue
            String body = new String(jsonObjReq.getBody());
            Log.d(TAG, "Advertising body: " + body);
            addToRequestQueue(jsonObjReq);
        }
        else
        {
            gwAdvertisingList = advertisingList;
            sendAuthenticationToken(ADVERTISING);
        }
    }


    private void processErrorServer(VolleyError error, String metodoLlamada )
    {
        String json = error.getMessage();

        try
        {
            if( json != null )
            {
                JSONObject jsonObject = new JSONObject(json);
//            JSONObject jsonError = jsonObject.getJSONObject("error");


                if ( jsonObject != null && jsonObject.has("message") )
                {
                    String message = "";
                    switch ( jsonObject.getInt("code") )
                    {
                        case 401:

                            message = jsonObject.getString("message");
                            if ( message.toLowerCase().contains("access token") )
                            {
                                sendAuthenticationToken(metodoLlamada);
                            }

                            break;
                        case 400:
                            message = jsonObject.getString("message");
                            Log.e(TAG, "400 error: " + message);
                            break;
                    }
                    //Additional cases
                }
            }
        }
        catch ( JSONException e )
        {
            e.printStackTrace();
        }
    }

}
