package com.gwella.androidlibrarybeacongwella.core;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.gwella.androidlibrarybeacongwella.model.GwAdvertising;
import com.gwella.androidlibrarybeacongwella.model.GwMetadata;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase Java que es un singleton para que el usuario solo pueda manejar los metodos que son publicos desde esta
 * clase.
 *
 * @author juangra on 31/8/16.
 */
public class ManagerGCM
{
    private static ManagerGCM mInstance = null;
    private Context context;

    private ManagerGCM(final Context context )
    {
        this.context = context;
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkAndRequestPermissions())
            {
                GwConstants.isInicialize = true;
                init(context);
            }
        }
        else {
            GwConstants.isInicialize = true;
            init(context);
        }
    }

    private void init(Context context)
    {
        GwConstants.inicializeLibrary(context);
    }

    private boolean checkAndRequestPermissions()
    {
        int storagePermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int permissionReadContact = ContextCompat.checkSelfPermission(context, Manifest.permission.GET_ACCOUNTS);

        int permissionLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

        ArrayList<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionReadContact != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.GET_ACCOUNTS);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        return listPermissionsNeeded.isEmpty();
    }

    public static synchronized ManagerGCM getInstance( Context context, String environment, String nameScreenMain )
    {
        if(mInstance == null)
        {
            new GwEnviroment().addEnviroment(environment);
            GwConstants.mainScreen = nameScreenMain;
            mInstance = new ManagerGCM(context);
//            GwConstants.enabledBluetooth();
        }
        return mInstance;
    }

    public static void verifyBluetooth(final Activity activity)
    {
        if( GwConstants.isInicialize )
        {
            if ( GwConstants.BEACONS_ACTIVE )
            {
                GwConstants.verifyBluetooth(activity);
            }
        }
    }

    public static void parseMessageIntent(final Bundle extras)
    {
        if( GwConstants.isInicialize )
        {
            GwConstants.parseMessageIntent(extras);
        }
    }

    public static void sendMetadata( Context context, List<GwMetadata> metadataList)
    {
        if( GwConstants.isInicialize )
        {
            HttpController.getInstance(context).sendMetadata(metadataList);
        }
    }

    public static void sendRegistriesAdver( Context context, List<GwAdvertising> advertisingList)
    {
        if( GwConstants.isInicialize )
        {
            HttpController.getInstance(context).sendMetadataAdvertising(advertisingList);
        }
    }

//    public static String getAlerts(Context context)
//    {
//        HttpController.getInstance(context).getAlerts(new VolleyCallback()
//        {
//            @Override
//            public String onSuccess(String result)
//            {
//                return result;
//            }
//
//            @Override
//            public void failed(Exception e)
//            {
//                return e.toString();
//            }
//        });
//
//    }

}
