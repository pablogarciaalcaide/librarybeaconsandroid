package com.gwella.androidlibrarybeacongwella.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.gwella.androidlibrarybeacongwella.R;
import com.gwella.androidlibrarybeacongwella.core.GwConstants;
import com.gwella.androidlibrarybeacongwella.core.GeofenceErrorMessages;
import com.gwella.androidlibrarybeacongwella.core.GwMiAdapterBD;
import com.gwella.androidlibrarybeacongwella.model.GwMessages;
import com.gwella.androidlibrarybeacongwella.model.GwNotifyTrigger;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;
import com.gwella.androidlibrarybeacongwella.model.GwRules;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Esta clase es un IntentService que es al que llama el GwGeofencesService cuando
 * detecta que hemos entrado o salido de una geocerca, en su metodo onHanldeIntent recibe el intent
 * con toda la informacion necesaria par en el metodo rangingGeofence trate el geofence y sus mensajes.
 *
 * @author juangra on 11/11/15.
 */
public class GwGeofencingIntentService extends IntentService
{

    protected static final String TAG = "GeofencingIntentService";

    /**
     * Constructor publico de la clase.
     */
    public GwGeofencingIntentService()
    {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Metodo generico del IntentService que recibe un intent con los datos para ser tratado.
     *
     * @param intent con todos los datos del geofence detectado.
     */
    @Override
    protected void onHandleIntent(Intent intent)
    {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceErrorMessages.getErrorString(this, geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );

            if( (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) && triggeringGeofences.get(0).getRequestId().equals("special") )
            {
                //Hacer el tratamiento de salir de la posicion y volver a crear geofences
                Log.d(TAG, "Salgo del geofence special ****************");

                Intent intentService = new Intent(getApplicationContext(), GwGeofencesService.class);
//                intentService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                stopService(intentService);

//                startActivity(intentService);
            }
            else
            {
                if( !triggeringGeofences.get(0).getRequestId().equals("special") )
                    rangingGeofence(triggeringGeofences, geofenceTransition);
            }

            Log.i(TAG, geofenceTransitionDetails);
        } else {
            // Log the error.
            Log.e(TAG, getString(R.string.geofence_transition_invalid_type, geofenceTransition));
        }
    }

    /**
     * Metodo que recibe dos parametros un listado de geofences detectados y un entero con el valor
     * de la transicion de este mismo para ser tratados sus mensajes asociados.
     *
     * @param triggeringGeofences List de geofences detectados, solo utilizaremos el primero.
     * @param transitionValue valor entero de la transicion del geofence, 1 = Enter, 2 = Exit.
     */
    private void rangingGeofence(List<Geofence> triggeringGeofences, int transitionValue)
    {
        final GwMiAdapterBD db = GwMiAdapterBD.getInstance(getApplicationContext());
        ScheduledExecutorService scheduled = Executors.newSingleThreadScheduledExecutor();

        final String geoUid = triggeringGeofences.get(0).getRequestId();

        if( transitionValue == Geofence.GEOFENCE_TRANSITION_ENTER )
            db.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_GEO, geoUid.replaceAll("_\\d+$", ""), GwConstants.K_ACTION_ENTER, "", GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));

        if( transitionValue == Geofence.GEOFENCE_TRANSITION_EXIT )
            db.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_GEO, geoUid.replaceAll("_\\d+$", ""), GwConstants.K_ACTION_EXIT, "", GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));

        final GwRules rule = howNextMessage(geoUid, transitionValue);

        if ( rule != null )
        {
            final GwMessages message = db.selectMessage(rule.getUidMessage());

            final GwNotifyTrigger notifyTrigger = new GwNotifyTrigger(geoUid, message.getUid(), 0, 0, System.currentTimeMillis());
            if( GwConstants.isPossibleLimitCampaingAndUser(message.getCampaign()) )
            {
                if( GwConstants.isDayOfWeek(rule.getWeekdays()) && GwConstants.isTime(rule.getStartTime(), rule.getEndTime()) )
                {
                    scheduled.schedule(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Log.e(TAG, "Envio notificacion entrada geofence; " + message.getTitle() + " - " + rule.get_id());
                            db.insertNotifyTrigger(notifyTrigger);
                            GwConstants.sendNotification(getApplicationContext(), message.getTitle(), message.getContent(), rule.getEnterInterval() + message.getId(), message.getUid(), geoUid.replaceAll("_\\d+$", ""), GwConstants.K_SOURCE_GEO);
                            //Guardar el registro de el servicio
                            JSONObject j_uidNotify = new JSONObject();
                            try
                            {
                                j_uidNotify.put("notification_uid", message.getUid());
                            }catch ( JSONException e )
                            {
                                e.printStackTrace();
                            }
                            db.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_GEO, geoUid.replaceAll("_\\d+$", ""), GwConstants.K_ACTION_SHOW, j_uidNotify.toString(), GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));
                        }
                    }, ( long ) rule.getEnterInterval(), TimeUnit.SECONDS);
                }
            }
        }
    }

    /**
     * Metodo que nos devuelve la siguiente regla que se tiene que tratar.
     *
     * @param geouid uuid del geofence a tratar.
     * @param transitionValue valor entero de la transicion del geofence para saber si es de entrada o de salida.
     * @return la regla a tratar o null si no hubiese mas para tratar.
     */
    private GwRules howNextMessage(String geouid, int transitionValue)
    {
        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
        ArrayList<GwRules> rules;
        if( transitionValue == Geofence.GEOFENCE_TRANSITION_ENTER )
        {
            rules = bd.selectAllRulesToChannelEnterRegion(geouid);
            Log.e(TAG, "Geofence rules enter");
        }
        else
        {
            rules = bd.selectAllRulesToChannelExitRegion(geouid);
            Log.e(TAG, "Geofence rules exit");
        }

        GwNotifyTrigger trigger = bd.selectLastNotifyTriggerGeo(geouid);

        if( trigger != null )
        {
            boolean exists = false;
            boolean isPossible = false;
            int pos = 0;
            for ( int i = 0; i < rules.size(); i++ )
            {
                if ( trigger.getKeyNotification().equals(rules.get(i).getUidMessage()) )
                {
                    exists = true;
                    pos = i;
                    long currentTime = System.currentTimeMillis();
                    long elapsedTime = currentTime - trigger.getTimeLaunch();

                    if ( rules.get(i).getRelaunchInterval() > - 1 && (elapsedTime / 1000) > rules.get(i).getRelaunchInterval() )
                    {
                        isPossible = true;
                    }
                }
            }

            if ( exists && isPossible )
            {
                //Por si es la última posición
                if ( pos == (rules.size() - 1) )
                {
                    return rules.get(0);
                }
                else
                {
                    return rules.get(pos + 1);
                }
            }
            else
            {
                return null;
            }
        }
        else
        {
            if( rules != null && rules.size() > 0 )
            {
                return rules.get(0);
            }
            else
            {
                return null;
            }
        }
    }

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param context               The app context.
     * @param geofenceTransition    The ID of the geofence transition.
     * @param triggeringGeofences   The geofence triggered.
     * @return                      The transition details formatted as String.
     */
    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList triggeringGeofencesIdsList = new ArrayList();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType    A transition type constant defined in Geofence
     * @return                  A String indicating the type of transition
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                return getString(R.string.geofence_transition_dwell);
            default:
                return getString(R.string.unknown_geofence_transition);
        }
    }
}
