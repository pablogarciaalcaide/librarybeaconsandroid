package com.gwella.androidlibrarybeacongwella.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.gwella.androidlibrarybeacongwella.core.GwConstants;
import com.gwella.androidlibrarybeacongwella.core.GwMiAdapterBD;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Esta clase es un IntentService que es al que llama el GCMBroadcastReceiver cuando captura un
 * mensaje PUSH para que el metodo onHandleIntent trate el mensaje.
 *
 * @author juangra on 10/12/15.
 */
public class GCMIntentService extends IntentService
{
    public GCMIntentService() {
        super("GCMIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getBaseContext());

        String messageType = gcm.getMessageType(intent);
        Bundle extras = intent.getExtras();

        if (!extras.isEmpty())
        {
            Log.i("GCMIntentService", extras.toString());
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
            {
                String pushUid = extras.getString("push_uid");

                if( GwConstants.getSharedPreferences(getApplicationContext(), pushUid, GwConstants.NAME_FILE_CONTROL_PUSH).length() == 0 )
                {
                    JSONObject j_uidNotify = new JSONObject();
                    try
                    {
                        j_uidNotify.put("notification_uid", extras.getString(GwConstants.MESSAGE_UID));
                    }catch ( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    bd.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_PUSH, pushUid, GwConstants.K_ACTION_RECEIVE, j_uidNotify.toString(), GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));
                    GwConstants.setSharedPreferences(getApplicationContext(), pushUid, "this", GwConstants.NAME_FILE_CONTROL_PUSH);
                    String campaignUid = extras.getString("campaign_uid");
                    if ( GwConstants.isPossibleLimitCampaingAndUser(campaignUid) )
                    {
                        try
                        {
                            JSONObject jMessage = new JSONObject(extras.getString("message"));
                            String idNotification = String.valueOf(System.currentTimeMillis());
                            String cuatroCaracteres = idNotification.substring(idNotification.length() - 4, idNotification.length());
                            GwConstants.sendNotificationPush(getBaseContext(), jMessage.getString("title"), jMessage.getString("content"), pushUid, Integer.parseInt(cuatroCaracteres), extras.getString(GwConstants.MESSAGE_UID), extras.getString("message"), campaignUid);

                            bd.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_PUSH, pushUid, GwConstants.K_ACTION_SHOW, j_uidNotify.toString(), GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));

                        }catch ( JSONException e )
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }
}
