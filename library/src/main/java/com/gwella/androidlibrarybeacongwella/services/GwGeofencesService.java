package com.gwella.androidlibrarybeacongwella.services;

import android.Manifest;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.gwella.androidlibrarybeacongwella.core.GwConstants;
import com.gwella.androidlibrarybeacongwella.core.GeofenceErrorMessages;
import com.gwella.androidlibrarybeacongwella.core.GwMiAdapterBD;
import com.gwella.androidlibrarybeacongwella.model.GwGeofence;

import java.util.ArrayList;

/**
 * Servicio que implementa GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
 * ResultCallback, y es el se utiliza para crear, identificar y escuchar los geofencing
 *
 * @author juangra on 4/4/16.
 */
public class GwGeofencesService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status>
{
    protected static final String TAG = "GwGeofencesService";
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * The list of geofences used in this sample.
     */
    protected ArrayList<Geofence> mGeofenceList;

    /**
     * Used to keep track of whether geofences were added.
     */
    private boolean mGeofencesAdded;

    /**
     * Radius of
     */
    private final float radius = 300f;

    private MyLocationListener myLocationListener;
    private MyLocationListener myLocationListener2;
    private LocationManager mLocationManager;


    boolean gps_enabled=false;
    boolean network_enabled=false;

    /**
     * Used when requesting to add or remove geofences.
     */
    private PendingIntent mGeofencePendingIntent;

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Empty list for storing geofences.
        mGeofenceList = new ArrayList<Geofence>();

        // Initially set the PendingIntent used in addGeofences() and removeGeofences() to null.
        mGeofencePendingIntent = null;

        mGeofencesAdded = true;

        /**
         * 1 - Obtener la localizacion
         * 2 - Crear Geo special y generar su cuadrado de posiciones.
         * 3 - Hacer select con posiciones para obtener los geos que esten dentro.
         * 4 -
         */
        addLastLocation();

        // Kick off the request to build GoogleApiClient.
        buildGoogleApiClient();
    }

    private void addLastLocation()
    {
        myLocationListener = new MyLocationListener();
        myLocationListener2 = new MyLocationListener();

        mLocationManager = ( LocationManager ) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //exceptions will be thrown if provider is not permitted.
        try{gps_enabled=mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
        try{network_enabled=mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}
        if ( ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
        {
            if( gps_enabled )
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, myLocationListener);
            if( network_enabled )
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, myLocationListener2);
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Cuando se destruye el servicio desconectamos del cliente de Google
     */
    @Override
    public void onDestroy() {
        mGoogleApiClient.disconnect();
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {

        if( mGeofenceList.size() > 0 )
        {
            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

            // Add the geofences to be monitored by geofencing service.
            builder.addGeofences(mGeofenceList);

            // Return a GeofencingRequest.
            return builder.build();
        }
        return null;
    }

    /**
     * Cuando se inicia el servicio conectamos con el cliente de Google
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
//        mGoogleApiClient.connect();
        return super.onStartCommand(intent, flags, startId);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        addGeofences();
        Log.i(TAG, "Connected to GoogleApiClient");
    }

    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost for some reason.
        Log.i(TAG, "Connection suspended");

        // onConnected() will be called again automatically when the service reconnects
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onResult(Status status) {
        if (status.isSuccess()) {
            // Update state and save in shared preferences.
            mGeofencesAdded = !mGeofencesAdded;
//            SharedPreferences.Editor editor = mSharedPreferences.edit();
//            editor.putBoolean(GwConstants.GEOFENCES_ADDED_KEY, mGeofencesAdded);
//            editor.apply();

        } else {
            // Get the status code for the error and log it using a user-friendly message.
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    status.getStatusCode());
            Log.e(TAG, errorMessage);
        }
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GwGeofencingIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * This sample hard codes geofence data. A real app might dynamically create geofences based on
     * the user location.
     */
    public void populateGeofenceList(final Location location) {

        mGeofenceList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId("special")

                // Set the circular region of this geofence.
                .setCircularRegion( location.getLatitude(), location.getLongitude(), radius)

                // Set the expiration duration of the geofence. This geofence gets automatically
                // removed after this period of time.
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setLoiteringDelay(1000)

                // Set the transition types of interest. Alerts are only generated for these
                // transition. We track entry and exit transitions in this sample.
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT)

                // Create the geofence.
                .build());

        ArrayList<GwGeofence> gwGeofences = checkGeofencesNearby(location);
        GwConstants.geofenceVisibles = gwGeofences;
        Log.i(TAG, "Geofences cercanos a monitorizar, " + gwGeofences.size());

        for (GwGeofence geo : gwGeofences) {
            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(geo.getUid())

                            // Set the circular region of this geofence.
                    .setCircularRegion( geo.getLatitude(), geo.getLongitude(), geo.getRadius())

                            // Set the expiration duration of the geofence. This geofence gets automatically
                            // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setLoiteringDelay(1000)

                            // Set the transition types of interest. Alerts are only generated for these
                            // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)

                            // Create the geofence.
                    .build());
        }

        mGoogleApiClient.connect();
    }

    private ArrayList<GwGeofence> checkGeofencesNearby(final Location location)
    {
        ArrayList<GwGeofence> geofencesCheck = new ArrayList<>();
        //Obtener los puntos para crear la consulta a la bbdd.
        PointF center = new PointF((float)location.getLatitude(), (float)location.getLongitude());
//        final double mult = 1; // mult = 1.1; is more reliable
//        PointF p1 = GwConstants.calculateDerivedPosition(center, mult * radius, 0);
//        PointF p2 = GwConstants.calculateDerivedPosition(center, mult * radius, 90);
//        PointF p3 = GwConstants.calculateDerivedPosition(center, mult * radius, 180);
//        PointF p4 = GwConstants.calculateDerivedPosition(center, mult * radius, 270);
//        Log.i(TAG, "Posicion central, " + location.getLatitude() + " , " + location.getLongitude());
//        Log.i(TAG, "Posiciones calculadas, P1: " + p1.x + "," + p1.y + " - P2: " + p2.x + "," + p2.y + " - P3: " + p3.x + "," + p3.y + " - P4: " + p4.x + "," + p4.y);

        GwMiAdapterBD db = GwMiAdapterBD.getInstance(getBaseContext());
        ArrayList<GwGeofence> gwGeofences = db.selectAllGeofences();

        for(GwGeofence gwGeofence : gwGeofences)
        {
            if( GwConstants.pointIsInCircle(new PointF(gwGeofence.getLatitude(), gwGeofence.getLongitude()), gwGeofence.getRadius(), center, radius) )
            {
                geofencesCheck.add(gwGeofence);
            }
            //Limito el numero de geofences que puede monitorizar la libreria a 10,
            //por el limite que impone Google de 100 geos por dispositivo y usuario.
            if( geofencesCheck.size() > 9 )
            {
                return geofencesCheck;
            }
        }

        return geofencesCheck;
    }

    private void logSecurityException(SecurityException securityException) {
        Log.e(TAG, "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }

    /**
     * Este metodo es para añadir los geofences al cliente de Google, primero comprobamos que
     * estamos conectados al cliente, creamos un GeofencingRequest con el metodo getGeofencingRequest
     * y lo añadimos al locationServices.GeofencingApi
     */
    public void addGeofences()
    {
        if (mGoogleApiClient.isConnected())
        {
            GeofencingRequest geofencingRequest = getGeofencingRequest();
            if( geofencingRequest != null )
            {
                try
                {
                    LocationServices.GeofencingApi.addGeofences(mGoogleApiClient,
                            // The GeofenceRequest object.
                            geofencingRequest,
                            // A pending intent that that is reused when calling removeGeofences(). This
                            // pending intent is used to generate an intent when a matched geofence
                            // transition is observed.
                            getGeofencePendingIntent()).setResultCallback(this); // Result processed in onResult().
                }catch ( SecurityException securityException )
                {
                    // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
                    logSecurityException(securityException);
                }
            }
        }
    }

    public void removeGeofences()
    {
        if (mGoogleApiClient.isConnected())
        {
            GeofencingRequest geofencingRequest = getGeofencingRequest();
            if( geofencingRequest != null )
            {
                try
                {
                    LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient,
                            // A pending intent that that is reused when calling removeGeofences(). This
                            // pending intent is used to generate an intent when a matched geofence
                            // transition is observed.
                            getGeofencePendingIntent()).setResultCallback(this); // Result processed in onResult().
                }catch ( SecurityException securityException )
                {
                    // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
                    logSecurityException(securityException);
                }
            }
        }
    }


    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location)
        {
            Log.i(TAG, "Llamo al metodo de enviar la location, " + location.getProvider());
            // Get the geofences used. Geofence data is hard coded in this sample.
            populateGeofenceList(location);

            if ( ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
            {
                mLocationManager.removeUpdates(myLocationListener);
                mLocationManager.removeUpdates(myLocationListener2);
                mLocationManager = null;
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.i("OnProviderDisabled", "OnProviderDisabled");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.i("onProviderEnabled", "onProviderEnabled");
        }

        @Override
        public void onStatusChanged(String provider, int status,
                                    Bundle extras) {
            Log.i("onStatusChanged", "onStatusChanged");

        }

    }
}
