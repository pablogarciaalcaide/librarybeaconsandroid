package com.gwella.androidlibrarybeacongwella.services;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * Esto es un servicio que recibe la captura de el mensaje PUSH que viene
 * del servidor de Google.
 *
 * @author juangra on 10/12/15.
 */
public class GCMBroadcastReceiver extends WakefulBroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        ComponentName comp =
                new ComponentName(context.getPackageName(),
                        GCMIntentService.class.getName());

        startWakefulService(context, (intent.setComponent(comp)));
    }
}
