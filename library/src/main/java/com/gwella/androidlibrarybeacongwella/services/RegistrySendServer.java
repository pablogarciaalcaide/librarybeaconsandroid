package com.gwella.androidlibrarybeacongwella.services;

import android.content.Context;
import android.util.Log;

import com.gwella.androidlibrarybeacongwella.core.HttpController;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Clase creada para lanzar una tarea cada 15 minutos para que se envien los registros que hubiese
 * marcados como no sincronizados. Utilizamos un Timer para crear el temporizador y en este lanzamos
 * un TimerTask para ejecutar la tarea.
 *
 * @author juangra on 15/12/15.
 */
public class RegistrySendServer
{
    private final String TAG = "RegistrySendServer";

    private Context context;
    private ArrayList<GwRegistry> registries = new ArrayList<>();

    // La tarea se lanza cada 15 minutos
    final int tiempoRepeticion15 = 900000;
    final int tiempoDelayed = 60000;

    /**
     * Constructor de la clase que le pasamos un parametro, el contexto de la app.
     *
     * @param context sirve para mas adelante en la tarea poder crear una instancia de HttpCrontroller.
     */
    public RegistrySendServer(Context context)
    {
        this.context = context;

        // Programamos para que se lance la tarea siempre cada 15 minutos
        Timer temporizador = new Timer();
        temporizador.scheduleAtFixedRate(new Tarea1(), tiempoDelayed, tiempoRepeticion15);
        Log.i( TAG, "Arranco el temporizador del envio de registros");

    }

    /**
     * TimerTask para crear una instancia de HttpController para llamar al metodo que se encarga de
     * enviar los registros marcados como no sincronizados.
     */
    class Tarea1 extends TimerTask
    {
        public void run()
        {
            HttpController.getInstance(context).crearJsonRegistrosAndSend();

        }
    }

}
