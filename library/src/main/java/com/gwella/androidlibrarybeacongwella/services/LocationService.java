package com.gwella.androidlibrarybeacongwella.services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.gwella.androidlibrarybeacongwella.core.HttpController;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Clase que se encarga de localizar la posicion gps del dispositivo y enviar esa posicion como un
 * registro de localizacion. Contiene una tarea asincrona para que nos devuelva la posicion
 * sin bloquear el hilo principal de la libreria.
 *
 * @author juangra on 2/11/15.
 */
public class LocationService
{
    private Context context;
    private StartLocationAsync startLocationAsync;

    // La tarea se lanza cada 15 minutos
    final int tiempoRepeticion15 = 900000;

    /**
     * Constructor de la clase con un parametro, el contexto de la app. Creamos un temporizador con un Timer
     * para lanzar un Timer
     *
     * @param context Le pasamos el contexto para poder utilizarlo en la clase.
     */
    public LocationService(Context context)
    {
        this.context = context;

        // Programamos para que se lance la tarea siempre cada 15 minutos
        if ( ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
        {
            Timer temporizador = new Timer();
            temporizador.scheduleAtFixedRate(new Tarea1(), 0, tiempoRepeticion15);
            Log.i("LocAndroid", "Arranco el temporizador");
        }

    }

    class Tarea1 extends TimerTask
    {
        public void run()
        {
            startLocationAsync = new StartLocationAsync();
            startLocationAsync.execute();
        }
    }

    public class StartLocationAsync extends AsyncTask<String, Integer, Void>
    {
        public boolean enviado = false;

        public LocationManager mLocationManager;
        public GwellaLocationListener mGwellaLocationListener;
        public GwellaLocationListener mGwellaLocationListener2;

        @Override
        protected void onPreExecute()
        {
            mGwellaLocationListener = new GwellaLocationListener();
            mGwellaLocationListener2 = new GwellaLocationListener();
            mLocationManager = ( LocationManager ) context.getSystemService(Context.LOCATION_SERVICE);

            Looper.prepare();
            if ( ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
            {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 900000, 0, mGwellaLocationListener);
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 900000, 0, mGwellaLocationListener2);
            }
            Looper.loop();

        }

        @Override
        protected void onCancelled()
        {
            System.out.println("Cancelled by user!");
            if ( ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
            {
                mLocationManager.removeUpdates(mGwellaLocationListener);
                mLocationManager.removeUpdates(mGwellaLocationListener2);
            }
        }


        @Override
        protected Void doInBackground(String... params) {
            // TODO Auto-generated method stub

            while ( !enviado ) {
                Log.i( "LocAndroid", "Estoy en el while" );
            }
            return null;
        }

        public class GwellaLocationListener implements LocationListener {

            @Override
            public void onLocationChanged(Location location) {

                Log.i("LocAndroid", "Llamo al metodo de enviar la location, " + location.getProvider());
                String coordinates = location.getLatitude() + " , " + location.getLongitude();
                HttpController.getInstance(context).sendLocation( coordinates );
//                sendLocation(location);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.i("OnProviderDisabled", "OnProviderDisabled");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.i("onProviderEnabled", "onProviderEnabled");
            }

            @Override
            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
                Log.i("onStatusChanged", "onStatusChanged");

            }

        }

    }

}
