package com.gwella.androidlibrarybeacongwella.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.gwella.androidlibrarybeacongwella.core.GwConstants;
import com.gwella.androidlibrarybeacongwella.core.GwMiAdapterBD;
import com.gwella.androidlibrarybeacongwella.core.HttpController;
import com.gwella.androidlibrarybeacongwella.model.GwBeaconActions;
import com.gwella.androidlibrarybeacongwella.model.GwBeacon;
import com.gwella.androidlibrarybeacongwella.model.GwAssociationNotifications;
import com.gwella.androidlibrarybeacongwella.model.GwMessages;
import com.gwella.androidlibrarybeacongwella.model.GwNotifyTrigger;
import com.gwella.androidlibrarybeacongwella.model.GwRegistry;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author juangra on 7/9/15.
 */
public class GwServiceMonitoring extends IntentService implements BeaconConsumer{

    protected static final String TAG = "GwellaService";
    private BeaconManager beaconManager;

    private Beacon beacon;
    public GwServiceMonitoring()
    {
        super("GwServiceMonitoring");
    }

    public static boolean isIntentServiceRunning = false;

//    private HttpController httpController = HttpController.getInstance(getBaseContext());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)
    {
        if(!isIntentServiceRunning) {
            isIntentServiceRunning = true;
        }
    }

    @Override
    public void onBeaconServiceConnect() {

        beaconManager.setRangeNotifier(new RangeNotifier()
        {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region)
            {
                if ( beacons.size() > 0 )
                {
                    Log.i(TAG, "Reconoce: " + String.valueOf(beacons.size()) );
                    GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
                    //Esto es para que solo tratemos aquellos beacon que esten configurados en el parse.
                    //Puede ser que haya mas beacons con el mismo uuid pero que no esten activados en el parse.
                    ArrayList<Beacon> beaconArrayList = new ArrayList<Beacon>();
                    for ( Beacon beacon : beacons )
                    {
                        GwBeacon beaconInfo = bd.selectBeacon(beacon.getId1().toString().toUpperCase(), beacon.getId2().toInt(), beacon.getId3().toInt());

                        if ( beaconInfo != null )
                        {
                            beaconArrayList.add(beacon);
                            //Insertar registro de distancia de los beacons
                            JSONObject j_distance = new JSONObject();
                            try
                            {
                                j_distance.put("distance", String.valueOf(beacon.getDistance()));
                            }catch ( JSONException e )
                            {
                                e.printStackTrace();
                            }
                            bd.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_BEACON, beaconInfo.getUid(), GwConstants.K_ACTION_DISTANCE, j_distance.toString(), GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));
                        }
                    }

                    //Mirar si ha salido alguno de region
                    GwConstants.checkBeaconExitRegion(beaconArrayList);

                    Beacon beaconMinDistance = null;
                    if( beaconArrayList.size() > 0 )
                    {
                        if( beaconArrayList.size() == 1 )
                            beaconMinDistance = beaconArrayList.get(0);
                        else
                        {
                            beaconMinDistance = beaconArrayList.get(0);
                            for ( int i = 1; i < beaconArrayList.size(); i++ )
                            {
                                if( beaconArrayList.get(i).getDistance() < beaconMinDistance.getDistance() )
                                {
                                    beaconMinDistance = beaconArrayList.get(i);
                                }
                            }

                        }
                        GwBeacon beaconInfo = bd.selectBeacon(beaconMinDistance.getId1().toString().toUpperCase(), beaconMinDistance.getId2().toInt(), beaconMinDistance.getId3().toInt());
                        Log.d(TAG, "Lanzo notificacion");
                        beacon = beaconMinDistance;
                        rangingBeacon(beaconInfo);
                    }
                }
            }
        });

    }

    private void rangingBeacon( final GwBeacon beaconInfo )
    {
        final GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
        ScheduledExecutorService scheduled = Executors.newSingleThreadScheduledExecutor();
        final ArrayList<GwMessages> gwNotifications = bd.selectNotificationEnter(beaconInfo.getUid());

        Log.i(TAG, "Notificacion: Major: " + beaconInfo.getMajor() + "; Minor: " + beaconInfo.getMinor() + "; Distance: " + beacon.getDistance() );


        //Saco el tiempo para añadirselo si es la primera vez o actualizarlo cuando sea posible
        long tiempo = System.currentTimeMillis();

        GwBeaconActions actionRanging = bd.selectBeaconActionsRanging(beaconInfo.getUuid().toUpperCase(), beaconInfo.getMajor(), beaconInfo.getMinor());

        GwBeaconActions actionEnter = bd.selectBeaconActionsEnter(beaconInfo.getUuid().toUpperCase(), beaconInfo.getMajor(), beaconInfo.getMinor());

        if( actionEnter != null )
        {
            if( actionRanging != null )
            {
                if( !GwConstants.getStatusBeacon(beaconInfo) )
                {
                    //Mirar cual es la notificacion siguiente para tratar
                    final GwMessages nextNotification = howNextNotification(beaconInfo);
                    if ( nextNotification != null )
                    {
                        Log.i(TAG, nextNotification.getUid() + " - " + nextNotification.getTitle());
                        //Tratar la nueva notificacion
                        if ( nextNotification.isShow() )
                        {
                            if ( nextNotification.getTitle() != null && nextNotification.getTitle().length() > 0 )
                            {
                                final GwNotifyTrigger notifyTrigger = new GwNotifyTrigger(GwConstants.getSharedPreferences(getBaseContext(), GwConstants.UUID_COMPANY), nextNotification.getUid(), beaconInfo.getMajor(), beaconInfo.getMinor(), System.currentTimeMillis());

                                GwConstants.upsertBeaconProcess(beaconInfo, true);
                                scheduled.schedule(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.e(TAG, "Envio notificacion entrada1; " + beaconInfo.getMajorString() + " - " + beaconInfo.getMinorString());
                                        if( isPossibleSendNotification( beaconInfo, nextNotification ) )
                                        {
                                            if( GwConstants.isPossibleLimitCampaingAndUser(nextNotification.getCampaign()) )
                                            {
                                                if ( GwConstants.isDayOfWeek(nextNotification.getWeekdays()) && GwConstants.isTime(nextNotification.getStartTime(), nextNotification.getEndTime()) )
                                                {
                                                    Log.e(TAG, "Entrada1 is possible");
                                                    GwAssociationNotifications beaconNotifications = bd.selectAssociationNotification(nextNotification.getId(), beaconInfo.getUid());
                                                    String idNotify = beaconInfo.getMinorString() + beaconNotifications.getOrder();
                                                    bd.insertNotifyTrigger(notifyTrigger);
                                                    GwConstants.sendNotification(getBaseContext(), nextNotification.getTitle(), nextNotification.getContent(), Integer.valueOf(idNotify), nextNotification.getUid(), beaconInfo.getUid(), GwConstants.K_SOURCE_BEACON);
                                                    //Guardar el registro de el servicio
                                                    JSONObject j_uidNotify = new JSONObject();
                                                    try
                                                    {
                                                        j_uidNotify.put("notification_uid", nextNotification.getUid());
                                                    }catch ( JSONException e )
                                                    {
                                                        e.printStackTrace();
                                                    }
                                                    bd.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_BEACON, beaconInfo.getUid(), GwConstants.K_ACTION_SHOW, j_uidNotify.toString(), GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));
                                                }
                                            }
                                        }
                                        GwConstants.upsertBeaconProcess(beaconInfo, false);
                                    }
                                }, ( long ) nextNotification.getEnterInterval(), TimeUnit.SECONDS);
                            }
                        }
                    }
                }
            }
            else
            {
                GwBeaconActions actionsPrimerRanging = new GwBeaconActions(
                        0,
                        beaconInfo.getUuid().toUpperCase(),
                        beaconInfo.getMajor(),
                        beaconInfo.getMinor(),
                        0,
                        0,
                        tiempo
                );

                bd.upsertBeaconActions(actionsPrimerRanging);

            }
        }
        else
        {
            //Si no existe creamos el action de entrada
            actionEnter = new GwBeaconActions(
                    0,
                    beaconInfo.getUuid().toUpperCase(),
                    beaconInfo.getMajor(),
                    beaconInfo.getMinor(),
                    0,
                    tiempo,
                    0
            );

            bd.upsertBeaconActions(actionEnter);

            //Guardar el registro de el servicio de entrada en el backend
            bd.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_BEACON, beaconInfo.getUid(), GwConstants.K_ACTION_ENTER, "", GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));

            //Mirar si tengo que enviar notificacion de entrada
            //Mirar cual es la notificacion siguiente para tratar
            final GwMessages nextNotification = howNextNotification(beaconInfo);
            if( nextNotification != null )
            {
                if ( nextNotification.isShow() )
                {
                    if ( nextNotification.getTitle() != null && nextNotification.getTitle().length() > 0 )
                    {
                        final GwNotifyTrigger notifyTrigger = new GwNotifyTrigger(GwConstants.getSharedPreferences(getBaseContext(), GwConstants.UUID_COMPANY), nextNotification.getUid(), beaconInfo.getMajor(), beaconInfo.getMinor(), System.currentTimeMillis());
                        GwConstants.upsertBeaconProcess(beaconInfo, true);
                        scheduled.schedule(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Log.e(TAG, "Envio notificacion entrada2; " + beaconInfo.getMajorString() + " - " + beaconInfo.getMinorString());
                                if ( isPossibleSendNotification(beaconInfo, nextNotification) )
                                {
                                    if( GwConstants.isPossibleLimitCampaingAndUser(nextNotification.getCampaign()) )
                                    {
                                        if ( GwConstants.isDayOfWeek(nextNotification.getWeekdays()) && GwConstants.isTime(nextNotification.getStartTime(), nextNotification.getEndTime()) )
                                        {
                                            Log.e(TAG, "Entrada2 is possible");
                                            GwAssociationNotifications beaconNotifications = bd.selectAssociationNotification(nextNotification.getId(), beaconInfo.getUid());
                                            String idNotify = beaconInfo.getMinorString() + beaconNotifications.getOrder();
                                            bd.insertNotifyTrigger(notifyTrigger);
                                            GwConstants.sendNotification(getBaseContext(), nextNotification.getTitle(), nextNotification.getContent(), Integer.valueOf(idNotify), nextNotification.getUid(), beaconInfo.getUid(), GwConstants.K_SOURCE_BEACON);
                                            //Guardar el registro de el servicio
                                            JSONObject j_uidNotify = new JSONObject();
                                            try
                                            {
                                                j_uidNotify.put("notification_uid", nextNotification.getUid());
                                            }catch ( JSONException e )
                                            {
                                                e.printStackTrace();
                                            }
                                            bd.insertRegistry(new GwRegistry(0, GwConstants.K_SOURCE_BEACON, beaconInfo.getUid(), GwConstants.K_ACTION_SHOW, j_uidNotify.toString(), GwConstants.formatingDateToServer(), false, System.currentTimeMillis()));
                                        }
                                    }
                                }
                                GwConstants.upsertBeaconProcess(beaconInfo, false);
                            }
                        }, ( long ) nextNotification.getEnterInterval(), TimeUnit.SECONDS);
                    }
                }
            }
        }

    }

    private GwMessages howNextNotification( GwBeacon beacon )
    {
        GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
        ArrayList<GwMessages> notifications = bd.selectNotificationEnter(beacon.getUid());
        GwMessages notificacion = null;

        if( notifications.size() > 0 )
        {
            int posicion = GwConstants.getSharedPreferencesInt(getBaseContext(), beacon.getUuid() + beacon.getMajorString() + beacon.getMinorString());

            for ( int i = posicion; i < notifications.size(); i++ )
            {
                GwNotifyTrigger trigger = bd.selectLastNotifyTrigger(beacon.getUuid(), beacon.getMajor(), beacon.getMinor(), notifications.get(i).getUid());
                if ( trigger != null )
                {
                    int segundos = notifications.get(i).getRelaunchInterval();
                    if( segundos == - 1 )
                    {
                        GwConstants.setSharedPreferences(getBaseContext(), beacon.getUuid() + beacon.getMajorString() + beacon.getMinorString(), i + 1);
                        return null;
                    }
                    if( segundos == 0 )
                    {
                        GwConstants.setSharedPreferences(getBaseContext(), beacon.getUuid() + beacon.getMajorString() + beacon.getMinorString(), i + 1);
                        notificacion = notifications.get(i);
                        return notificacion;
                    }
                    else
                    {
                        long tiempoActual = System.currentTimeMillis();
                        //Pasar los segundos a milisegundos
                        long mSegundosNotificacion = segundos * 1000;
                        long tTranscurrido = tiempoActual - trigger.getTimeLaunch();
                        if( tTranscurrido >= mSegundosNotificacion )
                        {
                            GwConstants.setSharedPreferences(getBaseContext(), beacon.getUuid() + beacon.getMajorString() + beacon.getMinorString(), i + 1);
                            notificacion = notifications.get(i);
                            return notificacion;
                        }
                    }

                }
                else{
                    GwConstants.setSharedPreferences(getBaseContext(), beacon.getUuid() + beacon.getMajorString() + beacon.getMinorString(), i + 1);
                    notificacion = notifications.get(i);
                    return notificacion;
                }
            }
            GwConstants.setSharedPreferences(getBaseContext(), beacon.getUuid() + beacon.getMajorString() + beacon.getMinorString(), 0);
            return null;
        }
        else
        {
            return null;
        }
    }

    private boolean isPossibleSendNotification( GwBeacon gwBeacon, GwMessages notifications )
    {
        if( beacon.getId2().toString().equals(gwBeacon.getMajorString()) && beacon.getId3().toString().equals(gwBeacon.getMinorString()) )
        {
            if( beacon.getDistance() <= notifications.getDistance() )
            {
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 )
        {
            startForeground(1, new Notification());
        }

        beaconManager = BeaconManager.getInstanceForApplication(getBaseContext());
        beaconManager.bind(this);
    }

    @Override
    public void onDestroy() {
        isIntentServiceRunning = false;
        super.onDestroy();
        beaconManager.unbind(this);
    }
}
