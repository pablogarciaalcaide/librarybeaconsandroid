package com.gwella.androidlibrarybeacongwella.model;


import org.altbeacon.beacon.Beacon;

/**
 * @author juangra on 6/10/15.
 */
public class GwCountBeaconRegion implements Comparable
{
    private int countEnterRegion = 0;
    private int countExitRegion = 0;
    private String uuid;
    private String major;
    private String minor;

    public GwCountBeaconRegion(int countEnterRegion, int countExitRegion, String uuid, String major, String minor)
    {
        this.countEnterRegion = countEnterRegion;
        this.countExitRegion = countExitRegion;
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
    }

    public int getCountEnterRegion()
    {
        return countEnterRegion;
    }

    public void setCountEnterRegion(int countEnterRegion)
    {
        this.countEnterRegion = countEnterRegion;
    }

    public int getCountExitRegion()
    {
        return countExitRegion;
    }

    public void setCountExitRegion(int countExitRegion)
    {
        this.countExitRegion = countExitRegion;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getMajor()
    {
        return major;
    }

    public void setMajor(String major)
    {
        this.major = major;
    }

    public String getMinor()
    {
        return minor;
    }

    public void setMinor(String minor)
    {
        this.minor = minor;
    }

    @Override
    public int compareTo(Object another)
    {
        if( another instanceof Beacon )
        {
            if( uuid.equals( ((Beacon)another).getId1().toString() ) &&
                    major.equals( ((Beacon)another).getId2().toString() ) &&
                    minor.equals( ((Beacon)another).getId3().toString() ) )
            {
                return 0;
            }
            else
                return -1;
        }
        return 1;
    }
}
