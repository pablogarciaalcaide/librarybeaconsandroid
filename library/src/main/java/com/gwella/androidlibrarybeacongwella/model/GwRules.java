package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 14/3/16.
 */
public class GwRules
{
    private int _id;
    private String uidChanel;
    private String campaign;
    private int distance;
    private int enterInterval;
    private int exitInterval;
    private int relaunchInterval;
    private String uidMessage;
    private String weekdays;
    private String startTime;
    private String endTime;

    public GwRules(){}

    public GwRules(int id, String uidChanel, String campaign, int distance, int enterInterval, int exitInterval, int relaunchInterval, String uidMessage, String weekdays, String startTime, String endTime)
    {
        super();
        _id = id;
        this.uidChanel = uidChanel;
        this.campaign = campaign;
        this.distance = distance;
        this.enterInterval = enterInterval;
        this.exitInterval = exitInterval;
        this.relaunchInterval = relaunchInterval;
        this.uidMessage = uidMessage;
        this.weekdays = weekdays;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUidChanel() {
        return uidChanel;
    }

    public void setUidChanel(String uidChanel) {
        this.uidChanel = uidChanel;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getEnterInterval() {
        return enterInterval;
    }

    public void setEnterInterval(int enterInterval) {
        this.enterInterval = enterInterval;
    }

    public int getExitInterval() {
        return exitInterval;
    }

    public void setExitInterval(int exitInterval) {
        this.exitInterval = exitInterval;
    }

    public int getRelaunchInterval() {
        return relaunchInterval;
    }

    public void setRelaunchInterval(int relaunchInterval) {
        this.relaunchInterval = relaunchInterval;
    }

    public String getUidMessage() {
        return uidMessage;
    }

    public void setUidMessage(String uidMessage) {
        this.uidMessage = uidMessage;
    }

    public String getWeekdays()
    {
        return weekdays;
    }

    public void setWeekdays(String weekdays)
    {
        this.weekdays = weekdays;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }
}
