package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 23/9/15.
 */
public class GwNotifyTrigger
{
    private String uuid;
    private String keyNotification;
    private int major;
    private int minor;
    private long timeLaunch;

    public GwNotifyTrigger(){}

    public GwNotifyTrigger(String uuid, String keyNotification, int major, int minor, long timeLaunch)
    {
        super();
        this.uuid = uuid;
        this.keyNotification = keyNotification;
        this.major = major;
        this.minor = minor;
        this.timeLaunch = timeLaunch;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getKeyNotification()
    {
        return keyNotification;
    }

    public void setKeyNotification(String keyNotification)
    {
        this.keyNotification = keyNotification;
    }

    public int getMajor()
    {
        return major;
    }

    public void setMajor(int major)
    {
        this.major = major;
    }

    public int getMinor()
    {
        return minor;
    }

    public void setMinor(int minor)
    {
        this.minor = minor;
    }

    public long getTimeLaunch()
    {
        return timeLaunch;
    }

    public void setTimeLaunch(long timeLaunch)
    {
        this.timeLaunch = timeLaunch;
    }
}
