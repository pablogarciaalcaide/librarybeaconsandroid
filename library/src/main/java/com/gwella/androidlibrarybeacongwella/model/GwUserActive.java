package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 25/11/15.
 */
public class GwUserActive
{
    private int _id;
    private String client_token;
    private String device_token;
    private String app_name;
    private String app_version;
    private String device_type;

    public GwUserActive(){}

    public GwUserActive(int id, String client_token, String device_token, String app_name, String app_version, String device_type)
    {
        super();
        _id = id;
        this.client_token = client_token;
        this.device_token = device_token;
        this.app_name = app_name;
        this.app_version = app_version;
        this.device_type = device_type;
    }

    public int get_id()
    {
        return _id;
    }

    public void set_id(int _id)
    {
        this._id = _id;
    }

    public String getClient_token()
    {
        return client_token;
    }

    public void setClient_token(String client_token)
    {
        this.client_token = client_token;
    }

    public String getDevice_token()
    {
        return device_token;
    }

    public void setDevice_token(String device_token)
    {
        this.device_token = device_token;
    }

    public String getApp_name()
    {
        return app_name;
    }

    public void setApp_name(String app_name)
    {
        this.app_name = app_name;
    }

    public String getApp_version()
    {
        return app_version;
    }

    public void setApp_version(String app_version)
    {
        this.app_version = app_version;
    }

    public String getDevice_type()
    {
        return device_type;
    }

    public void setDevice_type(String device_type)
    {
        this.device_type = device_type;
    }
}
