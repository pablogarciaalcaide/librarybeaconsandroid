package com.gwella.androidlibrarybeacongwella.model;


/**
 * @author juangra on 18/1/17.
 */

public class GwMetadata
{
    private String key;
    private String type;
    private Object value;

    public static final String TYPE_BOOLEAN = "boolean";
    public static final String TYPE_INTEGER = "integer";
    public static final String TYPE_FLOAT = "float";
    public static final String TYPE_STRING = "string";
    public static final String TYPE_DATE = "date";

    public GwMetadata(){}

    public GwMetadata(String key, String type, Object value)
    {
        super();
        this.key = key;
        this.type = type;
        this.value = value;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");

        sb.append(" \"key\": ").append("\"").append(key).append("\",\n");
        sb.append(" \"type\": ").append("\"").append(type).append("\",\n");
        sb.append(" \"value\": ").append("\"").append(value).append("\"\n");
        sb.append("}");
        return sb.toString();
    }
}
