package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 23/9/15.
 */
public class GwAssociationNotifications
{
    private String keyEvent;
    private int keyNotification;
    private int order;

    public GwAssociationNotifications(){}

    public GwAssociationNotifications(String keyEvent, int keyNotification, int order)
    {
        super();
        this.keyEvent = keyEvent;
        this.keyNotification = keyNotification;
        this.order = order;
    }

    public String getKeyEvent()
    {
        return keyEvent;
    }

    public void setKeyEvent(String keyEvent)
    {
        this.keyEvent = keyEvent;
    }

    public int getKeyNotification()
    {
        return keyNotification;
    }

    public void setKeyNotification(int keyNotification)
    {
        this.keyNotification = keyNotification;
    }

    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }
}
