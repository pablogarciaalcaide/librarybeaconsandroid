package com.gwella.androidlibrarybeacongwella.model;


/**
 * @author juangra on 25/9/15.
 */
public class GwRegistry
{
    private int _id;
    private String source = "";
    private String uid = "";
    private String action = "";
    private String data = "";// Se guarda un json con los parametros que se necesite enviar.
    private String dateCreated;
    private boolean sinchronizado;
    private Double synchronDate;

    public GwRegistry()
    {}

    public GwRegistry(int _id, String source, String uid, String action, String data, String dateCreated, boolean sinchronizado, double synchronDate)
    {
        super();
        this._id = _id;
        this.source = source;
        this.uid = uid;
        this.action = action;
        this.data = data;
        this.sinchronizado = sinchronizado;
        this.dateCreated = dateCreated;
        this.synchronDate = synchronDate;
    }

    public int get_id()
    {
        return _id;
    }

    public void set_id(int _id)
    {
        this._id = _id;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getData()
    {
        return data != null ? data : "";
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public boolean isSinchronizado()
    {
        return sinchronizado;
    }

    public void setSinchronizado(boolean sinchronizado)
    {
        this.sinchronizado = sinchronizado;
    }

    public String getDateCreated()
    {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public double getSynchronDate()
    {
        return synchronDate;
    }

    public void setSynchronDate(double synchronDate)
    {
        this.synchronDate = synchronDate;
    }

}
