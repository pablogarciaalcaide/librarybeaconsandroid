package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 2/9/15.
 */
public class GwBeaconActions {

    private int id;
    private String UUID;
    private int major;
    private int minor;
    private long last_exit;
    private long last_enter;
    private long last_range_notify;


    public GwBeaconActions()
    {}

    public GwBeaconActions(int id, String UUID, int major, int minor, long last_exit, long last_enter, long last_range_notify)
    {
        super();
        this.id = id;
        this.UUID = UUID;
        this.major = major;
        this.minor = minor;
        this.last_exit = last_exit;
        this.last_enter = last_enter;
        this.last_range_notify = last_range_notify;
    }

    public GwBeaconActions(int id, String UUID, int major, int minor, String last_exit, String last_enter, String last_range_notify)
    {
        super();
        this.id = id;
        this.UUID = UUID;
        this.major = major;
        this.minor = minor;
        this.last_exit = Long.parseLong(last_exit);
        this.last_enter = Long.parseLong(last_enter);
        this.last_range_notify = Long.parseLong(last_range_notify);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }



    public long getLast_exit() {
        return last_exit;
    }

    public void setLast_exit(long last_exit) {
        this.last_exit = last_exit;
    }

    public long getLast_enter() {
        return last_enter;
    }

    public void setLast_enter(long last_enter) {
        this.last_enter = last_enter;
    }

    public long getLast_range_notify() {
        return last_range_notify;
    }

    public void setLast_range_notify(long last_range_notify) {
        this.last_range_notify = last_range_notify;
    }

}
