package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 3/9/15.
 */
public class GwBeacon
{
    private String uuid;
    private int major;
    private int minor;
    private String uid;
    private boolean isMonitoring;

    public GwBeacon(){}

    public GwBeacon(String uuid, int major, int minor, String uid, boolean isMonitoring)
    {
        super();
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
        this.uid = uid;
        this.isMonitoring = isMonitoring;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getMajor() {
        return major;
    }

    public String getMajorString()
    {
        return String.valueOf(major);
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public String getMinorString()
    {
        return String.valueOf(minor);
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public boolean isMonitoring()
    {
        return isMonitoring;
    }

    public void setMonitoring(boolean monitoring)
    {
        isMonitoring = monitoring;
    }
}
