package com.gwella.androidlibrarybeacongwella.model;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author juangra on 16/12/15.
 */
public class GwGeofence
{
    private int _id;
    private String uid;
    private String type;
    private float latitude;
    private float longitude;
    private int radius;

    public GwGeofence(){}

    public GwGeofence(int id, String uid, String type, float latitude, float longitude, int radius)
    {
        super();
        _id = id;
        this.uid = uid;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public int get_id()
    {
        return _id;
    }

    public void set_id(int _id)
    {
        this._id = _id;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public float getLatitude()
    {
        return latitude;
    }

    public void setLatitude(float latitude)
    {
        this.latitude = latitude;
    }

    public float getLongitude()
    {
        return longitude;
    }

    public void setLongitude(float longitude)
    {
        this.longitude = longitude;
    }

    public int getRadius()
    {
        return radius;
    }

    public void setRadius(int radius)
    {
        this.radius = radius;
    }
}
