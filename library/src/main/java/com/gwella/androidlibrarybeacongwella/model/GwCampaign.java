package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 31/3/16.
 */
public class GwCampaign
{
    private int id;
    private String uid;
    private String name;
    private long startDate;
    private long enDate;
    private int messagesLimitCampaign;
    private int messagesCountCampaign;
    private int messagesLimitUser;

    public GwCampaign(int id, String uid, String name, long startDate, long enDate, int messagesLimitCampaign, int messagesCountCampaign, int messagesLimitUser)
    {
        this.id = id;
        this.uid = uid;
        this.name = name;
        this.startDate = startDate;
        this.enDate = enDate;
        this.messagesLimitCampaign = messagesLimitCampaign;
        this.messagesCountCampaign = messagesCountCampaign;
        this.messagesLimitUser = messagesLimitUser;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getStartDate()
    {
        return startDate;
    }

    public void setStartDate(long startDate)
    {
        this.startDate = startDate;
    }

    public long getEnDate()
    {
        return enDate;
    }

    public void setEnDate(long enDate)
    {
        this.enDate = enDate;
    }

    public int getMessagesLimitCampaign()
    {
        return messagesLimitCampaign;
    }

    public void setMessagesLimitCampaign(int messagesLimitCampaign)
    {
        this.messagesLimitCampaign = messagesLimitCampaign;
    }

    public int getMessagesCountCampaign()
    {
        return messagesCountCampaign;
    }

    public void setMessagesCountCampaign(int messagesCountCampaign)
    {
        this.messagesCountCampaign = messagesCountCampaign;
    }

    public int getMessagesLimitUser()
    {
        return messagesLimitUser;
    }

    public void setMessagesLimitUser(int messagesLimitUser)
    {
        this.messagesLimitUser = messagesLimitUser;
    }
}
