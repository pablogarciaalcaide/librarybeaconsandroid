package com.gwella.androidlibrarybeacongwella.model;

/**
 * @author juangra on 23/9/15.
 */
public class GwMessages
{
    private int id;
    private String uid;
    private boolean show;
    private String title;
    private String content;
    private int enterInterval;
    private int exitInterval;
    private int distance;
    private int relaunchInterval;
    private String viewId;
    private String url;
    private String imagePath;
    private String campaign;
    private String weekdays;
    private String startTime;
    private String endTime;
    private String imageNotifPath;
    private String sound;

    public GwMessages(){}

    public GwMessages(int id, String uid, boolean show, String title, String content, int enterInterval, int exitInterval, int distance, int relaunchInterval, String viewId, String url, String imagePath, String campaign, String weekdays, String startTime, String endTime, String imageNotifPath, String sound)
    {
        super();
        this.id = id;
        this.uid = uid;
        this.show = show;
        this.title = title;
        this.content = content;
        this.enterInterval = enterInterval;
        this.exitInterval = exitInterval;
        this.distance = distance;
        this.relaunchInterval = relaunchInterval;
        this.viewId = viewId;
        this.url = url;
        this.imagePath = imagePath;
        this.campaign = campaign;
        this.weekdays = weekdays;
        this.startTime = startTime;
        this.endTime = endTime;
        this.imageNotifPath = imageNotifPath;
        this.sound = sound;
    }

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getEnterInterval()
    {
        return enterInterval;
    }

    public void setEnterInterval(int enterInterval)
    {
        this.enterInterval = enterInterval;
    }

    public int getExitInterval()
    {
        return exitInterval;
    }

    public void setExitInterval(int exitInterval)
    {
        this.exitInterval = exitInterval;
    }

    public int getDistance()
    {
        return distance;
    }

    public void setDistance(int distance)
    {
        this.distance = distance;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public int getRelaunchInterval()
    {
        return relaunchInterval;
    }

    public void setRelaunchInterval(int relaunchInterval)
    {
        this.relaunchInterval = relaunchInterval;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getViewId()
    {
        return viewId;
    }

    public void setViewId(String viewId)
    {
        this.viewId = viewId;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    public String getCampaign()
    {
        return campaign;
    }

    public void setCampaign(String campaign)
    {
        this.campaign = campaign;
    }

    public String getWeekdays()
    {
        return weekdays;
    }

    public void setWeekdays(String weekdays)
    {
        this.weekdays = weekdays;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }

    public String getImageNotifPath()
    {
        return imageNotifPath;
    }

    public void setImageNotifPath(String imageNotifPath)
    {
        this.imageNotifPath = imageNotifPath;
    }

    public String getSound()
    {
        return sound;
    }

    public void setSound(String sound)
    {
        this.sound = sound;
    }
}
