package com.example.pruebabeaconsmonitoring;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.gwella.androidlibrarybeacongwella.core.GwConstants;
import com.gwella.androidlibrarybeacongwella.core.GwMiAdapterBD;
import com.gwella.androidlibrarybeacongwella.core.ManagerGCM;
import com.gwella.androidlibrarybeacongwella.model.GwAdvertising;
import com.gwella.androidlibrarybeacongwella.model.GwGeofence;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    // Google Map
    private GoogleMap gMap;

    private ManagerGCM managerGCM;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar toolbar = ( Toolbar ) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


        (( MapFragment ) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback()
        {
            @Override
            public void onMapReady(GoogleMap googleMap)
            {
                gMap = googleMap;
                if ( ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
                {
                    gMap.setMyLocationEnabled(true);
                }
                gMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener()
                {
                    @Override
                    public boolean onMyLocationButtonClick()
                    {
                        paintGeosVisibles();
                        return false;
                    }
                });

            }
        });



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                paintGeosVisibles();
//                List<GwMetadata> metadataList = new ArrayList<GwMetadata>();
//                GwMetadata metadata = new GwMetadata();
//                metadata.setKey("user");
//                metadata.setType(GwMetadata.TYPE_STRING);
//                metadata.setValue("Esto es una prueba");
//                metadataList.add(metadata);
//
//                metadata = new GwMetadata();
//                metadata.setKey("edad");
//                metadata.setType(GwMetadata.TYPE_INTEGER);
//                metadata.setValue(15);
//                metadataList.add(metadata);
//
//                managerGCM.sendMetadata(metadataList);

                List<GwAdvertising> advertisingList = new ArrayList<>();
                GwAdvertising advertising = new GwAdvertising("age", GwAdvertising.TYPE_INTEGER, 37, getApplicationContext());
                advertisingList.add(advertising);
                advertising = new GwAdvertising("sex", GwAdvertising.TYPE_STRING, "M", getApplicationContext());
                advertisingList.add(advertising);
                advertising = new GwAdvertising("ANDROID_ID", GwAdvertising.TYPE_STRING, GwConstants.getSecureAndroidId(), getApplicationContext());
                advertisingList.add(advertising);


                ManagerGCM.sendRegistriesAdver(getApplicationContext(), advertisingList);
            }
        });

//        HttpController.getInstance(getApplicationContext()).getAlerts(new VolleyCallback()
//        {
//            @Override
//            public void onSuccess(String result)
//            {
//                Log.d("MainActivity", "Alerts: " + result);
//            }
//
//            @Override
//            public void failed(Exception e)
//            {
//                Log.e("MainActivity", "Error recuperando las alertas", e);
//            }
//        });

        /**
         * Esta llamada es para que el usuario encienda el BT para poder monotorizar los beacons
         */
        ManagerGCM.verifyBluetooth(this);

        /**
         * Esto hay que implementarlo para que las notificaciones puedan ser tratadas
         * Parte 1
         */
        Bundle extras = getIntent().getExtras();
        if ( extras != null )
        {
            ManagerGCM.parseMessageIntent(extras);
        }
    }

    private void paintGeosVisibles()
    {
        gMap.clear();

        if( GwConstants.isInicialize )
        {
            GwMiAdapterBD bd = GwMiAdapterBD.getInstance(getApplicationContext());
            ArrayList<GwGeofence> geofences = bd.selectAllGeofences();
            if ( geofences.size() > 0 )
            {
                for ( GwGeofence geo : geofences )
                {
                    LatLng latLng = new LatLng(geo.getLatitude(), geo.getLongitude());
                    gMap.addCircle(new CircleOptions().center(latLng).radius(geo.getRadius()).fillColor(0x50ec984a).strokeWidth(0));
                    Log.i("MainMap", "Pinto un circulo");
                }
            }

            if ( GwConstants.geofenceVisibles.size() > 0 )
            {
                for ( GwGeofence geo : GwConstants.geofenceVisibles )
                {
                    LatLng latLng = new LatLng(geo.getLatitude(), geo.getLongitude());
                    gMap.addCircle(new CircleOptions().center(latLng).radius(geo.getRadius()).fillColor(0x5084f15c).strokeWidth(0));
                    Log.i("MainMap", "Pinto un circulo");
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
