package com.example.pruebabeaconsmonitoring;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by juangra on 6/3/18.
 */

public class SplashActivity extends AppCompatActivity
{
    private final int PERMISSION_ALL = 200;
    private Bundle dataBundle;

    @Override
    public void onCreate(Bundle saveinstancestate)
    {
        super.onCreate(saveinstancestate);
        setContentView(R.layout.splash_activity);

        onNewIntent(getIntent());

        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkAndRequestPermissions()) {
                init();
            }
        } else {
            init();
        }
    }

    @Override
    public void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if( extras != null )
        {
            dataBundle = extras;
        }
    }

    private boolean checkAndRequestPermissions()
    {
        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int permissionReadContact = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);

        int permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        ArrayList<String> listPermissionsNeeded = new ArrayList<>();
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionReadContact != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.GET_ACCOUNTS);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_ALL);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        switch(requestCode){

            case PERMISSION_ALL:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init();
                }
                else
                {
                    //Permission distinct Granted Successfully. Write working code here.
                    Toast.makeText(SplashActivity.this, "Tienes que aceptar todos los permisos", Toast.LENGTH_LONG).show();
                    init();
                }
                break;
        }
    }

    private void init()
    {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if(dataBundle != null)
        {
            intent.putExtras(dataBundle);
        }
        startActivity(intent);
        finish();
    }
}
