package com.example.pruebabeaconsmonitoring;

import android.app.Application;

import com.gwella.androidlibrarybeacongwella.core.GwConstants;
import com.gwella.androidlibrarybeacongwella.core.GwEnviroment;
import com.gwella.androidlibrarybeacongwella.core.ManagerGCM;


/**
 * Created by juangra on 21/9/15.
 */
public class GlobalApplication extends Application {

    public static final String TAG = "GlobalApplication";


    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * Esto es para capar el monitoreo de beacons
         */
        GwConstants.BEACONS_ACTIVE = true;

        /**
         * Esta es la llamada principal a la libreria para ponerla en marcha.
         * Siempre se iniciará en una clase que extienda de Application.
         * Se le pasa tambien el tipo de entorno que vayamos a utilizar.
         * Se le pasa el nombre de la actividad principal donde recuperaremos el intent para tratarlo.
         */
        //TODO: Desarrollo
        ManagerGCM.getInstance(getBaseContext(), GwConstants.ENVIROMENT_DEVELOPMENT, "MainActivity");
        //TODO: Produccion
//        ManagerGCM.getInstance(getBaseContext(), GwConstants.ENVIROMENT_PRODUCTION, "MainActivity");
    }
}
